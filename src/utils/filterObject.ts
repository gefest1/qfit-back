export const filterObject = <T>(
  obj: T,
  predicate: (
    value: [keyof T, any],
    index: number,
    array: [keyof T, any][],
  ) => boolean,
) =>
  Object.fromEntries(
    (Object.entries(obj) as [keyof T, any][]).filter(predicate),
  ) as Partial<T>;

// filter<S extends T>(predicate: (value: T, index: number, array: T[]) => value is S, thisArg?: any): S[];

// filter(predicate: (value: T, index: number, array: T[]) => unknown, thisArg?: any): T[];
