export const getCorrectWeekDay = (weekDay: number) => {
  return (weekDay + 6) % 7;
};
