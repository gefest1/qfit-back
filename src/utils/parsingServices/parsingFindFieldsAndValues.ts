export const turnFindArgsIntoObject = async function (args: {
  fields: string[];
  values: any[];
}) {
  const { fields, values } = args;
  const findQuery: any = {};
  fields.map((val, ind) => {
    if (values[ind] === undefined) return;
    findQuery[val] = values[ind];
  });
  return findQuery;
};
