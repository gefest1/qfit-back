import { Inject, Injectable } from '@nestjs/common';
import { Db, ObjectId } from 'mongodb';
import { TokenService } from 'src/modules/token/token.service';
import { CreateOwner } from '../models/createOwner.model';
import { Owner } from '../models/owner.interface';
import * as bcrypt from 'bcryptjs';
import { AWSPhotoUploadService } from 'src/modules/uploadPhoto/awsSpace.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class OwnerService {
  constructor(
    private photoUpload: AWSPhotoUploadService,
    private configService: ConfigService,
    private tokenService: TokenService,
    @Inject('DATABASE_CONNECTION')
    private db: Db,
  ) {}

  get getCollection() {
    return this.db.collection('owner');
  }

  async findOne(args: { owner: Partial<Owner> }): Promise<Owner> {
    const { owner } = args;
    const ownerResponce = (await this.getCollection.findOne(owner)) as Owner;
    return ownerResponce;
  }

  async createOwner(newOwner: CreateOwner): Promise<Owner> {
    const { fullName, email, phoneNumber, password, ownedClubID, avatar } =
      newOwner;
    const photoURL =
      avatar &&
      (await this.photoUpload.uploadImageAWS(
        (await avatar).createReadStream(),
      ));
    const clubID = new ObjectId(ownedClubID);
    const filteredPhoneNumber = phoneNumber.replace(/\D/g, '');
    const passwordHASH = await bcrypt.hash(password, 12);
    const owner: Partial<Owner> = {
      fullName,
      email,
      phoneNumber: filteredPhoneNumber,
      passwordHASH,
      photoURL,
      ownedClubID: clubID,
    };
    const currentDate = new Date();
    const createOwner = await this.getCollection.insertOne(owner);
    const token = this.tokenService.create({
      user: {
        _id: createOwner.insertedId,
        email: email,
        phoneNumber: phoneNumber,
        dateAdded: currentDate,
      },
    });
    owner._id = createOwner.insertedId;
    owner.token = token;
    return owner as Owner;
  }

  async login(args: { email: string; password: string }) {
    const { email, password } = args;
    const owner = await this.findOne({ owner: { email } });
    const checkPassword = await bcrypt.compare(password, owner.passwordHASH);
    if (!checkPassword) throw 'validation failed';
    const currentDate = new Date();
    const token = this.tokenService.create({
      user: {
        email: owner.email,
        _id: owner._id,
        phoneNumber: owner.phoneNumber,
        dateAdded: currentDate,
      },
    });
    owner.token = token;
    return owner;
  }
}
