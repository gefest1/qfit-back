import { Field, ObjectType } from '@nestjs/graphql';
import { PhotoUrlGraph } from 'src/modules/uploadPhoto/models/photo.model';
import { Modify } from 'src/utils/modifyType';
import { Owner } from './owner.interface';

@ObjectType('Owner')
export class OwnerGraph
  implements
    Modify<
      Omit<Owner, 'passwordHASH'>,
      {
        _id: string;
        ownedClubID: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  fullName: string;

  @Field()
  email: string;

  @Field()
  phoneNumber: string;

  @Field()
  token: string;

  @Field(() => PhotoUrlGraph)
  photoURL: PhotoUrlGraph;

  @Field()
  isAdmin: boolean;

  @Field()
  ownedClubID: string;

  @Field()
  dateAdded: Date;

  constructor(owner: Owner) {
    if (owner._id) this._id = owner._id.toHexString();
    if (owner.fullName) this.fullName = owner.fullName;
    if (owner.email) this.email = owner.email;
    if (owner.phoneNumber) this.phoneNumber = owner.phoneNumber;
    if (owner.token) this.token = owner.token;
    if (owner.photoURL) this.photoURL = owner.photoURL;
    if (owner.isAdmin) this.isAdmin = owner.isAdmin;
    if (owner.ownedClubID) this.ownedClubID = owner.ownedClubID.toHexString();
    if (owner.dateAdded) this.dateAdded = owner.dateAdded;
  }
}
