import { ArgsType, Field } from '@nestjs/graphql';
import { FileUpload, GraphQLUpload } from 'graphql-upload';
import { Modify } from 'src/utils/modifyType';
import { Owner } from './owner.interface';

@ArgsType()
export class CreateOwner
  implements Modify<Partial<Owner>, { ownedClubID: string }>
{
  @Field()
  fullName: string;

  @Field()
  email: string;

  @Field()
  phoneNumber: string;

  @Field({ nullable: false })
  password: string;

  @Field()
  ownedClubID: string;

  @Field(() => GraphQLUpload, {
    name: 'avatar',
    nullable: true,
  })
  avatar: Promise<FileUpload>;
}
