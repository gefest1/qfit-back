import { ObjectId } from 'mongodb';
import { User } from 'src/modules/user/models/user.model';

export type Owner = Omit<
  User,
  | 'dateOfBirth'
  | 'sex'
  | 'creditCard'
  | 'invitedBy'
  | 'promocode'
  | 'bonus'
  | 'youInvited'
> & {
  passwordHASH: string;
};
