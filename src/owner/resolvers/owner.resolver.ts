import { UseGuards } from '@nestjs/common';
import {
  Args,
  Context,
  GraphQLExecutionContext,
  Mutation,
  Resolver,
  Query,
} from '@nestjs/graphql';
import {
  CurrentUserGraph,
  PreAuthOwnerGuardGraph,
} from 'src/modules/admin/admin.guard';
import { CreateOwner } from '../models/createOwner.model';
import { OwnerGraph } from '../models/owner.model';
import { OwnerService } from '../services/owner.services';

@Resolver()
@UseGuards(PreAuthOwnerGuardGraph)
export class OwnerResolver {
  constructor(private ownerService: OwnerService) {}

  @Mutation(() => OwnerGraph)
  async registerOwner(@Args() args: CreateOwner, @Context() ctx) {
    const insertOwner = await this.ownerService.createOwner({
      ...args,
    });
    const ownerResponce = new OwnerGraph({
      ...insertOwner,
    });
    return ownerResponce;
  }

  @Query(() => OwnerGraph)
  async loginOwner(
    @Args('email', { type: () => String }) email: string,
    @Args('password', { type: () => String }) password: string,
  ) {
    const loginOwner = await this.ownerService.login({ email, password });
    const ownerResponce = new OwnerGraph({
      ...loginOwner,
    });
    return ownerResponce;
  }
}
