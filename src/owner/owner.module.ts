import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { PreAuthOwnerGuardGraph } from 'src/modules/admin/admin.guard';
import { TokenModule } from 'src/modules/token/token.module';
import { TokenService } from 'src/modules/token/token.service';
import { AWSPhotoUploadModule } from 'src/modules/uploadPhoto/awsSpace.module';
import { UserService } from 'src/modules/user/services/user.service';
import { MongoDBModule } from 'src/utils/mongo/mongo.module';
import { OwnerResolver } from './resolvers/owner.resolver';
import { OwnerService } from './services/owner.services';

@Module({
  imports: [AWSPhotoUploadModule, ConfigModule, TokenModule, MongoDBModule],
  providers: [OwnerService, OwnerResolver],
  exports: [OwnerService],
})
export class OwnerModule {}
