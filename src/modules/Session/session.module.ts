import { Module } from '@nestjs/common';
import { PreAuthModule } from '../admin/admin.module';
import { BookingModule } from '../booking/booking.module';
import { PaymentModule } from '../payment/payment.module';
import { ServiceModule } from '../service/service.module';
import { UserModule } from '../user/user.module';
import { SessionStackService } from './services/sessionStack.service';
import { SessionResolver } from './session.resolver';
import { FinishTrainingService } from './services/finishTraining.service';
import { StartTrainingService } from './services/startTraining.service';
import { UserSessionResolver } from './resolvers/user.session.resolver';
import { OwnerSessionResolver } from './resolvers/owner.session.resolver';
import { CreditTrainingService } from './services/credit.service';

@Module({
  imports: [
    ServiceModule,
    BookingModule,
    UserModule,
    PaymentModule,
    PreAuthModule,
    ServiceModule,
    BookingModule,
    PaymentModule,
  ],
  providers: [
    SessionResolver,
    SessionStackService,
    StartTrainingService,
    FinishTrainingService,
    CreditTrainingService,
    UserSessionResolver,
    OwnerSessionResolver,
  ],
  exports: [],
})
export class SessionModule {}
