import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';

import { ObjectId } from 'mongodb';
import { PaymentService } from 'src/modules/payment/services/payment.service';
import { ServiceGraph } from 'src/modules/service/models/service.model';
import { User } from 'src/modules/user/models/user.model';
import { UserService } from 'src/modules/user/services/user.service';
import { SessionGraph } from '../models/session.model';
import {
  SessionStack,
  SessionStackGraph,
  SessionStackStatus,
} from '../models/sessionStack.model';
import { Transaction } from '../models/transaction.model';
import {
  SessionStackService,
  SessionWithService,
} from './sessionStack.service';

@Injectable()
export class FinishTrainingService {
  constructor(
    private paymentService: PaymentService,
    private sessionStackService: SessionStackService,
    private userService: UserService,
  ) {}

  async updateStack(__: {
    stack: SessionWithService[];
    fullStack: SessionWithService[];
    endTime: Date;
    sessionStackId: ObjectId;
    generalPrice: number;
    bonusAmount?: number;
    status?: SessionStackStatus;
  }): Promise<SessionStack> {
    const {
      stack,
      endTime,
      sessionStackId,
      generalPrice,
      fullStack,
      bonusAmount,
      status,
    } = __;
    const fulledStack: SessionWithService[] = stack.map((e) => {
      return {
        ...e,
        endTime,
        duration: endTime.getTime() - e.startTime.getTime(),
      };
    });
    const vars = {
      val: {
        $arrayElemAt: [
          {
            $filter: {
              input: fulledStack.map((el) => {
                return {
                  _id: el._id,
                  endTime: el.endTime,
                  duration: el.duration,
                };
              }),
              as: 'key_val_pair_arr',
              cond: { $eq: ['$$key_val_pair_arr._id', '$_id'] },
            },
          },
          0,
        ],
      },
    };

    const [sessionStackUpdated] = await Promise.all([
      this.sessionStackService.sessionStackCollection.findOneAndUpdate(
        {
          _id: sessionStackId,
        },
        {
          $set: {
            stackIds: [
              ...fullStack.map((e) => e._id),
              ...stack.reverse().map((e) => e._id),
            ],
            endTime,
            generalPrice,
            bonusAmount,
            status,
          },
        },

        {
          returnDocument: 'after',
        },
      ),
      this.sessionStackService.sessionCollection.updateMany(
        {
          _id: {
            $in: stack.map((e) => e._id),
          },
        },
        [
          {
            $set: {
              endTime: {
                $let: {
                  vars,
                  in: '$$val.endTime',
                },
              },
              duration: {
                $let: {
                  vars,
                  in: '$$val.duration',
                },
              },
            },
          },
        ],
      ),
    ]);
    return sessionStackUpdated.value;
  }

  async finishTraining(__: {
    user: User;
    clubId: ObjectId;
  }): Promise<SessionStackGraph> {
    const { user, clubId } = __;
    const sessionStack = await this.sessionStackService.getSessionStackByUser(
      user._id,
    );

    if (clubId == sessionStack.clubId) throw 'CLUB ID IS WRONG';

    const stack: SessionWithService[] = [];
    sessionStack?.stack.forEach((el) => {
      if (!stack.length) {
        stack.push(el);
        return;
      }
      const _lastElement = stack[stack.length - 1];
      if (el._id == _lastElement._id) {
        stack.pop();
      }
      stack.push(_lastElement);
    });

    const _realTimeEnd = new Date();

    const [sessionStackFirst] = sessionStack.stack;

    let generalPrice =
      Math.max(
        Math.floor(
          (_realTimeEnd.getTime() - sessionStackFirst.startTime.getTime()) /
            1000 /
            60,
        ),
        1,
      ) * sessionStackFirst.service.price;
    if (sessionStack.freeTraining) {
      generalPrice = 0;
    }
    if (sessionStack.bonus) {
      if (!user.bonus) throw new ApolloError('you have no bonus');
      if (user.bonus > generalPrice) {
        user.bonus -= generalPrice;
        generalPrice = 0;
        await this.userService.updateOne({
          findUser: { _id: user._id },
          updateUser: { bonus: user.bonus },
          method: '$set',
        });
      }
      if (user.bonus <= generalPrice) {
        generalPrice -= user.bonus;
        await this.userService.updateOne({
          findUser: { _id: user._id },
          updateUser: { bonus: 0 },
          method: '$set',
        });
      }
    }
    this.userService.updateOne({
      findUser: { _id: user._id },
      updateUser: { bonus: generalPrice * 0.03 },
      method: '$inc',
    });

    const transactions = sessionStack.freeTraining
      ? null
      : await this.payTraining({
          generalPrice,
          transactions: sessionStack.transactions,
          user,
          sessionStackId: sessionStack._id,
        });
    this.updateStack({
      endTime: _realTimeEnd,
      generalPrice: generalPrice,
      sessionStackId: sessionStack._id,
      stack,
      fullStack: sessionStack.stack,
      bonusAmount: generalPrice * 0.03,
      status:
        transactions &&
        (transactions[transactions.length - 1].status == 'error'
          ? SessionStackStatus.credit
          : SessionStackStatus.success),
    });
    return new SessionStackGraph({
      ...sessionStack,
      endTime: _realTimeEnd,
      generalPrice,
      transactions,
      stack: sessionStack.stack.map(
        (_val) =>
          new SessionGraph({
            ..._val,
            endTime: _val.endTime ?? _realTimeEnd,
            duration:
              (_val.endTime ?? _realTimeEnd).getTime() -
              _val.startTime.getTime(),
            service: new ServiceGraph(_val.service),
          }),
      ),
    });
  }

  async initTransaction(__: {
    generalPrice: number;
    transactions: Transaction[];
    user: User;
  }): Promise<Transaction> {
    const { generalPrice, transactions, user } = __;
    const [firstTransaction] = transactions;
    if (generalPrice > firstTransaction.amount) {
      await this.paymentService.confirmPayment({
        Amount: firstTransaction.amount,
        TransactionId: firstTransaction.TransactionId,
      });
      return { ...firstTransaction, status: 'accepted' };
    } else {
      await this.paymentService.cancelPayment({
        TransactionId: firstTransaction.TransactionId,
      });
      return {
        TransactionId: firstTransaction.TransactionId,
        amount: firstTransaction.amount,
        status: 'canceled',
        userId: user._id,
        _id: firstTransaction._id,
      };
    }
  }

  async payTraining(__: {
    generalPrice: number;
    transactions: Transaction[];
    user: User;
    sessionStackId: ObjectId;
  }): Promise<Transaction[]> {
    const { generalPrice, transactions, user, sessionStackId } = __;
    const [firstTransaction] = transactions;

    const newFirstTransaction = await this.initTransaction(__);

    let transaction: Transaction;
    if (generalPrice != 0) {
      try {
        const result = await this.paymentService.chargePayment({
          userId: user._id.toHexString(),
          Token: user.creditCard[0].cardToken,
          Amount:
            generalPrice -
            (newFirstTransaction.status == 'accepted'
              ? newFirstTransaction.amount
              : 0),
        });
        transaction = {
          userId: user._id,
          status: 'accepted',
          TransactionId: result.Model.TransactionId,
          amount: generalPrice,
          _id: new ObjectId(),
        };
      } catch (e) {
        transaction = {
          userId: user._id,
          status: 'error',
          TransactionId: null,
          amount: generalPrice,
          _id: new ObjectId(),
        };
      }
    } else {
      transaction = {
        userId: user._id,
        status: 'accepted',
        TransactionId: null,
        amount: generalPrice,
        _id: new ObjectId(),
      };
    }

    this.sessionStackService.sessionStackCollection.findOneAndUpdate(
      {
        _id: sessionStackId,
      },
      {
        $set: {
          transactions: [newFirstTransaction, transaction],
        },
      },
      {
        returnDocument: 'after',
      },
    );
    return [firstTransaction, transaction];
  }
}
