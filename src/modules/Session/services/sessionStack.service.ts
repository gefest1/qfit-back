import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import { UserBookingService } from 'src/modules/booking/services/user.booking.service';
import { Service } from 'src/modules/service/models/service.model';
import { User } from 'src/modules/user/models/user.model';
import { Session } from '../models/session.model';
import { SessionStack } from '../models/sessionStack.model';

export type SessionWithService = Session & {
  service: Service;
};

@Injectable()
export class SessionStackService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
    public bookingService: UserBookingService,
  ) {}

  get sessionStackCollection(): Collection<SessionStack> {
    return this._mongodDb.collection('sessionStack');
  }

  get sessionCollection(): Collection<Session> {
    return this._mongodDb.collection('session');
  }

  getSessionStackCursorByFields<T extends SessionStack>(
    args: {
      findFields: (keyof SessionStack)[];
      findValues: any[];
    },
    options?: {
      withClub?: boolean;
      withUser?: boolean;
      withStack?: boolean;
    },
    addPipilines?: Document[],
  ) {
    const query: any = {};
    const _addPipilines = addPipilines ?? [];
    const { findFields, findValues } = args;
    findFields.map((val, ind) => {
      query[val] = findValues[ind];
    });
    const _showProj = {
      _id: 1,
      clubId: 1,
      endTime: 1,
      generalPrice: 1,
      stackIds: 1,
      startTime: 1,
      transactions: 1,
      userId: 1,
    };
    if (!!options?.withUser) {
      _showProj['user'] = {
        $arrayElemAt: ['$user', 0],
      };
    }
    if (!!options?.withClub) {
      _showProj['club'] = {
        $arrayElemAt: ['$club', 0],
      };
    }
    if (!!options?.withStack) {
      _showProj['stack'] = 1;
    }
    const sessionStackCursor = this.sessionStackCollection.aggregate<T>(
      [
        { $match: query },
        options?.withStack && {
          $lookup: {
            from: 'session',
            let: {
              stack_array: '$stackIds',
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $in: ['$_id', '$$stack_array'],
                  },
                },
              },
              {
                $lookup: {
                  from: 'service',
                  localField: 'serviceId',
                  foreignField: '_id',
                  as: 'service',
                },
              },
            ],
            as: 'stack',
          },
        },
        options?.withUser && {
          $lookup: {
            from: 'user',
            localField: 'userId',
            foreignField: '_id',
            as: 'user',
          },
        },
        options?.withClub && {
          $lookup: {
            from: 'club',
            localField: 'clubId',
            foreignField: '_id',
            as: 'club',
          },
        },
        {
          $project: _showProj,
        },
        ..._addPipilines,
      ].filter((_e) => !!_e),
    );
    return sessionStackCursor;
  }
  async insertSessionStack(__: SessionStack): Promise<SessionStack | void> {
    await this.sessionStackCollection.insertOne(__);
    return __;
  }

  async insertSession(__: Session): Promise<Session | void> {
    await this.sessionCollection.insertOne(__);
    return __;
  }

  async getSessionStackByUser(userId: ObjectId) {
    const _sessionStacks = await this.sessionStackCollection
      .aggregate<
        SessionStack & {
          stack: SessionWithService[];
        }
      >([
        { $match: { userId, endTime: null } },
        {
          $lookup: {
            from: 'session',
            let: {
              stack_array: '$stackIds',
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $in: ['$_id', '$$stack_array'],
                  },
                },
              },
              {
                $lookup: {
                  from: 'service',
                  localField: 'serviceId',
                  foreignField: '_id',
                  as: 'service',
                },
              },
              {
                $unwind: '$service',
              },
            ],
            as: 'stack',
          },
        },
        {
          $lookup: {
            from: 'club',
            localField: 'clubId',
            foreignField: '_id',
            as: 'club',
          },
        },
        {
          $unwind: '$club',
        },
        { $limit: 1 },
      ])
      .toArray();
    if (!_sessionStacks || !_sessionStacks.length) return;
    return _sessionStacks[0];
  }
}
