import { Injectable } from '@nestjs/common';
import { ObjectId } from 'mongodb';
import { PaymentService } from 'src/modules/payment/services/payment.service';
import { UserService } from 'src/modules/user/services/user.service';
import { SessionStack } from '../models/sessionStack.model';
import { Transaction } from '../models/transaction.model';
import { SessionStackService } from './sessionStack.service';

@Injectable()
export class CreditTrainingService {
  constructor(
    private paymentService: PaymentService,
    private sessionStackService: SessionStackService,
    private userService: UserService,
  ) {}

  async getCreditSession(userId: ObjectId): Promise<SessionStack[]> {
    return await this.sessionStackService.sessionStackCollection
      .find({
        status: 'credit',
        userId,
      })
      .toArray();
  }
  async clodeDebt(__: {
    sessionStackIds: ObjectId[];
    transaction: Transaction;
  }) {
    try {
      const { sessionStackIds, transaction } = __;
      await this.sessionStackService.sessionStackCollection.updateMany(
        {
          _id: {
            $in: sessionStackIds,
          },
        },
        {
          $set: {
            status: 'success',
          },
          $push: {
            transactions: transaction,
          },
        },
      );
    } catch (e) {}
  }
}
