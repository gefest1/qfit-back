import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { ObjectId } from 'mongodb';
import { UserBookingService } from 'src/modules/booking/services/user.booking.service';
import { PaymentService } from 'src/modules/payment/services/payment.service';
import { UserServiceService } from 'src/modules/service/services/userService';
import { User } from 'src/modules/user/models/user.model';
import { UserService } from 'src/modules/user/services/user.service';
import { Session, SessionGraph } from '../models/session.model';
import { SessionStackGraph } from '../models/sessionStack.model';
import { Transaction } from '../models/transaction.model';
import { SessionStackService } from './sessionStack.service';

@Injectable()
export class StartTrainingService {
  constructor(
    private userServiceService: UserServiceService,
    private bookingService: UserBookingService,
    private paymentService: PaymentService,
    private sessionStackService: SessionStackService,
    private userService: UserService,
  ) {}

  async strartTraing(
    user: User,
    serviceId: string,
    clubId: string,
    bookingId: string,
    duration: number,
    useBonus: boolean,
    userFreeTraining: boolean,
  ) {
    if (useBonus && userFreeTraining)
      throw new ApolloError('choose only bonus or free training');
    if (userFreeTraining && !user.freeTrainings)
      throw new ApolloError('you have no free trainings');
    const _booking =
      bookingId &&
      (await this.bookingService.getBookingWithClubAndService({
        bookingId,
      }));
    const service = !!bookingId
      ? _booking.service
      : await this.userServiceService.getServiceById({
          id: serviceId,
        });
    const realTime = new Date();
    const prePrice = Math.max(500, duration * service.price);

    const result =
      !userFreeTraining &&
      (await this.paymentService.authPayment({
        userId: user._id,
        token: user.creditCard[0].cardToken,
        amount: prePrice,
      }));

    if (result && !result.Success)
      throw new ApolloError(result.Model.CardHolderMessage);
    if (userFreeTraining) {
      this.userService.updateOne({
        findUser: { _id: user._id },
        updateUser: { freeTrainings: -1 },
        method: '$inc',
      });
    }
    const sessionStackId = new ObjectId();

    const transaction: Transaction = {
      userId: user._id,
      amount: prePrice,
      status: 'auth',
      TransactionId: result && result.Model.TransactionId,
    };

    const _sessionNote: Session = {
      _id: new ObjectId(),
      startTime: realTime,
      serviceId: service._id,
      userId: user._id,
    };

    const [sessionStack, session] = await Promise.all([
      this.sessionStackService.insertSessionStack({
        _id: sessionStackId,
        clubId: new ObjectId(clubId),
        userId: user._id,
        startTime: realTime,
        transactions: [transaction],
        stackIds: [_sessionNote._id],
        freeTraining: userFreeTraining && true,
        bonus: useBonus && true,
      }),
      this.sessionStackService.insertSession(_sessionNote),
      bookingId &&
        this.bookingService.updateBooking({
          _id: _booking._id,
          stackId: sessionStackId,
        }),
    ]);
    if (!session) throw new ApolloError('Error on create SESSION');

    return new SessionStackGraph({
      ...sessionStack,
      stack: [new SessionGraph(session)],
    });
  }
}
