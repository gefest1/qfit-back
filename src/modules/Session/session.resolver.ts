import { UseGuards } from '@nestjs/common';
import { Resolver, Mutation, Query, Args, Int } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';

import { CurrentUserGraph, PreAuthGuardGraph } from '../admin/admin.guard';
import { BookingGraph } from '../booking/models/booking.model';
import { UserBookingService } from '../booking/services/user.booking.service';
import { ClubGraph } from '../club/models';
import { PaymentService } from '../payment/services/payment.service';
import { ServiceGraph } from '../service/models/service.model';
import { UserServiceService } from '../service/services/userService';
import { User } from '../user/models/user.model';
import { QRCodeServiceGraph } from './models/QrCodeService.model';

import { SessionStackGraph } from './models/sessionStack.model';
import { FinishTrainingService } from './services/finishTraining.service';

import { SessionStackService } from './services/sessionStack.service';
import { StartTrainingService } from './services/startTraining.service';

@Resolver()
@UseGuards(PreAuthGuardGraph)
export class SessionResolver {
  constructor(
    public userServiceService: UserServiceService,
    public bookingService: UserBookingService,
    public paymentService: PaymentService,
    public sessionStackService: SessionStackService,
    public startTrainingService: StartTrainingService,
    private finishTrainingService: FinishTrainingService,
  ) {}

  @Mutation(() => SessionStackGraph, { nullable: true })
  async testSessionStack(): Promise<SessionStackGraph | void> {
    return null;
  }

  @Query(() => QRCodeServiceGraph)
  async QRCodeService(
    @CurrentUserGraph() user: User,
    @Args('clubId', {
      type: () => String,
    })
    clubId,
  ): Promise<QRCodeServiceGraph> {
    const [closestBooking, _services] = await Promise.all([
      this.bookingService.getClosestBooking({
        date: new Date(),
        userId: user._id,
      }),
      this.userServiceService.getCurrentDefaultServiceByDate({
        clubId: clubId,
        date: new Date(),
      }),
    ]);

    return new QRCodeServiceGraph({
      service:
        _services &&
        new ServiceGraph({
          ..._services,
          club: new ClubGraph(_services.club),
        }),
      booking:
        closestBooking &&
        new BookingGraph({
          ...closestBooking,
          service:
            closestBooking.service && new ServiceGraph(closestBooking.service),
          club: closestBooking.club && new ClubGraph(closestBooking.club),
        }),
    });
  }

  @Mutation(() => SessionStackGraph)
  async startSession(
    @CurrentUserGraph() user: User,
    @Args('serviceId', { type: () => String }) serviceId: string,
    @Args('clubId', { type: () => String }) clubId: string,
    @Args('bookingId', { type: () => String, nullable: true })
    bookingId: string,
    @Args('duration', { type: () => Int }) duration: number,
    @Args('useBonus', { type: () => Boolean, nullable: true })
    useBonus: boolean,
    @Args('useFreeTrainings', { type: () => Boolean, nullable: true })
    useFreeTrainings: boolean,
  ): Promise<SessionStackGraph> {
    return this.startTrainingService.strartTraing(
      user,
      serviceId,
      clubId,
      bookingId,
      duration,
      useBonus,
      useFreeTrainings,
    );
  }

  @Mutation(() => SessionStackGraph)
  async finishTraining(
    @CurrentUserGraph() user: User,
    @Args('clubId') clubId: string,
  ): Promise<SessionStackGraph | void> {
    return this.finishTrainingService.finishTraining({
      clubId: new ObjectId(clubId),
      user,
    });
  }
}
