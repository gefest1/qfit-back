import { UseGuards } from '@nestjs/common';
import { Args, Float, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import {
  CurrentUserGraph,
  PreAuthOwnerGuardGraph,
} from 'src/modules/admin/admin.guard';
import {
  Service,
  ServiceGraph,
} from 'src/modules/service/models/service.model';
import { User, UserGraph } from 'src/modules/user/models/user.model';
import { UserService } from 'src/modules/user/services/user.service';
import { Owner } from 'src/owner/models/owner.interface';
import { paginate } from 'src/utils/paginate';
import {
  Session,
  SessionAddictive,
  SessionGraph,
} from '../models/session.model';
import { SessionStack, SessionStackGraph } from '../models/sessionStack.model';
import { FinishTrainingService } from '../services/finishTraining.service';
import { SessionStackService } from '../services/sessionStack.service';
import { StartTrainingService } from '../services/startTraining.service';

@Resolver()
@UseGuards(PreAuthOwnerGuardGraph)
export class OwnerSessionResolver {
  constructor(
    private sessionStackService: SessionStackService,
    private startTrainingService: StartTrainingService,
    private userService: UserService,
    private finishTrainingService: FinishTrainingService,
  ) {}

  @Query(() => [SessionStackGraph])
  async getHistoryOfVisitations(
    @CurrentUserGraph() user: Owner,
    @Args('page', { type: () => Int }) page: number,
  ): Promise<SessionStackGraph[]> {
    const historyCursor =
      this.sessionStackService.getSessionStackCursorByFields<
        { user: User } & SessionStack
      >(
        {
          findFields: ['clubId', 'endTime'],
          findValues: [user.ownedClubID, { $exists: true }],
        },
        {
          withUser: true,
        },
      );
    console.log(historyCursor);
    const history = await paginate({
      cursor: historyCursor.sort({
        endTime: -1,
      }),
      page,
      elementsPerPage: 8,
    });
    // const historyGraph = history.map((val) => {
    //   return new SessionStackGraph({ ...val, stack: null });
    // });
    return history.map((val) => {
      return new SessionStackGraph({
        ...val,
        user: new UserGraph({ ...val.user, creditCard: undefined }),
      });
    });
  }

  @Query(() => [SessionStackGraph])
  async getCurrentSessionsOwner(
    @CurrentUserGraph() owner: Owner,
    @Args('page', { type: () => Int }) page: number,
  ) /*: Promise<SessionStackGraph[]> */ {
    const sessionStackCursor =
      this.sessionStackService.getSessionStackCursorByFields<
        { stack: Session[] & SessionAddictive[]; user: User } & SessionStack
      >(
        {
          findFields: ['clubId', 'endTime'],
          findValues: [owner.ownedClubID, { $exists: false }],
        },
        {
          withStack: true,
          withUser: true,
        },
      );
    const sessionStack = await paginate({
      cursor: sessionStackCursor,
      page,
      elementsPerPage: 8,
    });
    console.log(sessionStack[0].stack[0]);
    const sessionStackResponce = sessionStack.map((val) => {
      const userGraph = new UserGraph({ ...val.user, creditCard: null });
      const stackGraph = val.stack.map(
        (val: Session & { service: Service[] }) =>
          new SessionGraph({
            ...val,
            service: new ServiceGraph({ ...val.service[0] }),
          }),
      );
      const sessionStackGraph = new SessionStackGraph({
        ...val,
        user: userGraph,
        stack: stackGraph,
      });
      return sessionStackGraph;
    });
    return sessionStackResponce;
  }

  @Mutation(() => SessionStackGraph)
  async startSessionOwner(
    @CurrentUserGraph() owner: Owner,
    @Args('serviceId', { type: () => String }) serviceId: string,
    @Args('bookingId', { type: () => String, nullable: true })
    bookingId: string,
    @Args('duration', { type: () => Float }) duration: number,
    @Args('userId', { type: () => String }) userId: string,
    @Args('useBonus', { type: () => Boolean, nullable: true })
    useBonus: boolean,
    @Args('useFreeTrainings', { type: () => Boolean, nullable: true })
    useFreeTrainings: boolean,
  ): Promise<SessionStackGraph> {
    const user = await this.userService.findOne({
      user: { _id: new ObjectId(userId) },
    });
    const training = await this.startTrainingService.strartTraing(
      user,
      serviceId,
      owner.ownedClubID.toHexString(),
      bookingId,
      duration,
      useBonus,
      useFreeTrainings,
    );
    return training;
  }

  @Mutation(() => SessionStackGraph)
  async finishTrainingOwner(
    @CurrentUserGraph() owner: Owner,
    @Args('userId', { type: () => String }) userId: string,
  ) {
    const user = await this.userService.findOne({
      user: { _id: new ObjectId(userId) },
    });
    const finishTraining = await this.finishTrainingService.finishTraining({
      clubId: owner.ownedClubID,
      user,
    });
    return finishTraining;
  }
}
