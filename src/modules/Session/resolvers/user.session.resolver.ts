import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Mutation } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { bool } from 'sharp';
import {
  CurrentUserGraph,
  PreAuthGuardGraph,
} from 'src/modules/admin/admin.guard';
import { Club, ClubGraph } from 'src/modules/club/models';
import { ServiceGraph } from 'src/modules/service/models/service.model';
import { User } from 'src/modules/user/models/user.model';
import { PaymentService } from '../../payment/services/payment.service';
import { SessionGraph } from '../models/session.model';
import { SessionStack, SessionStackGraph } from '../models/sessionStack.model';
import { CreditTrainingService } from '../services/credit.service';
import { SessionStackService } from '../services/sessionStack.service';

@Resolver()
@UseGuards(PreAuthGuardGraph)
export class UserSessionResolver {
  constructor(
    public sessionStackService: SessionStackService,
    public creditTrainingService: CreditTrainingService,
    public paymentService: PaymentService,
  ) {}

  @Query(() => [SessionStackGraph], { nullable: true })
  async getHisory(
    @CurrentUserGraph() user: User,
  ): Promise<SessionStackGraph[]> {
    const sessionStacks = await this.sessionStackService
      .getSessionStackCursorByFields<{ club: Club } & SessionStack>(
        {
          findFields: ['userId', 'endTime'],
          findValues: [user._id, { $exists: true }],
        },
        {
          withClub: true,
        },
      )
      .sort({
        endTime: -1,
      })
      .limit(20)
      .toArray();
    this.sessionStackService.sessionStackCollection.find({}, {});
    console.log(sessionStacks);
    return sessionStacks.map((_val) => {
      return new SessionStackGraph({
        ..._val,
        club: new ClubGraph(_val.club),
      });
    });
  }

  @Query(() => SessionStackGraph, { nullable: true })
  async getCurrentSessionStack(
    @CurrentUserGraph() user: User,
  ): Promise<SessionStackGraph | void> {
    const _sessionStack = await this.sessionStackService.getSessionStackByUser(
      user._id,
    );

    return (
      _sessionStack &&
      new SessionStackGraph({
        ..._sessionStack,
        stack: _sessionStack.stack.map(
          (e) =>
            new SessionGraph({ ...e, service: new ServiceGraph(e.service) }),
        ),
      })
    );
  }

  @Query(() => [SessionStackGraph])
  async getCreditSession(
    @CurrentUserGraph() user: User,
  ): Promise<SessionStackGraph[] | void> {
    const sessionStacks = await this.creditTrainingService.getCreditSession(
      user._id,
    );
    return sessionStacks.map(
      (e) =>
        new SessionStackGraph({
          ...e,
          debt: e.transactions.reduce(
            (prev, current) =>
              prev + (current.status == 'error' ? current.amount : 0),
            0,
          ),
        }),
    );
  }

  @Mutation(() => Boolean)
  async payDebt(@CurrentUserGraph() user: User): Promise<boolean> {
    const sessionStacks = await this.creditTrainingService.getCreditSession(
      user._id,
    );
    const debt = sessionStacks.reduce(
      (prevAmount, e) =>
        prevAmount +
        e.transactions.reduce(
          (prev, current) =>
            prev + (current.status == 'error' ? current.amount : 0),
          0,
        ),
      0,
    );

    const transaction = await this.paymentService.chargePayment({
      userId: user._id.toHexString(),
      Token: user.creditCard[0].cardToken,
      Amount: debt,
    });

    this.creditTrainingService.clodeDebt({
      sessionStackIds: sessionStacks.map((e) => e._id),
      transaction: {
        amount: debt,
        status: 'credit',
        userId: user._id,
        TransactionId: transaction.Model.TransactionId,
        _id: new ObjectId(),
      },
    });
    return true;
  }
}
