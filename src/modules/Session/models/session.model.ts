import { Field, Float, GraphQLISODateTime, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import {
  Service,
  ServiceGraph,
} from 'src/modules/service/models/service.model';
import { User } from 'src/modules/user/models/user.model';
import { Modify } from 'src/utils/modifyType';

export interface Session {
  _id: ObjectId;
  serviceId: ObjectId;
  userId: ObjectId;
  startTime: Date;
  endTime?: Date;
  duration?: number;
}

export interface SessionAddictive {
  service: Service;
  user: User;
}

@ObjectType('Session')
export class SessionGraph
  implements
    Modify<
      Session,
      {
        _id: string;
        serviceId: string;
        userId: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  serviceId: string;

  @Field()
  userId: string;

  @Field(() => GraphQLISODateTime)
  startTime: Date;

  @Field(() => GraphQLISODateTime, { nullable: true })
  endTime?: Date;

  @Field(() => Float, { nullable: true })
  duration?: number;

  @Field(() => ServiceGraph, { nullable: true })
  service?: ServiceGraph;

  constructor(__: Partial<Session> & { service?: ServiceGraph }) {
    if (__._id != null) this._id = __._id.toHexString();
    if (__.serviceId != null) this.serviceId = __.serviceId.toHexString();
    if (__.userId != null) this.userId = __.userId.toHexString();
    if (__.startTime != null) this.startTime = __.startTime;
    if (__.endTime != null) this.endTime = __.endTime;
    if (__.duration != null) this.duration = __.duration;
    if (__.service != null) this.service = __.service;
  }
}
