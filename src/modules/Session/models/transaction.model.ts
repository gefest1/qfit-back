import { Field, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { Modify } from 'src/utils/modifyType';

export interface Transaction {
  _id?: ObjectId;
  userId: ObjectId;
  amount: number;
  TransactionId?: number;
  status: 'canceled' | 'error' | 'accepted' | 'auth' | 'credit';
}

@ObjectType()
export class TransactionGraph
  implements
    Modify<
      Transaction,
      {
        _id: string;
        userId: string;
      }
    >
{
  @Field()
  _id: string;
  @Field()
  userId: string;
  @Field()
  amount: number;
  @Field()
  TransactionId: number;
  @Field()
  status: 'canceled' | 'error' | 'accepted' | 'auth' | 'credit';
}
