import { Field, ObjectType } from '@nestjs/graphql';
import { BookingGraph } from 'src/modules/booking/models/booking.model';
import { ServiceGraph } from 'src/modules/service/models/service.model';

@ObjectType('QRCodeServiceResult')
export class QRCodeServiceGraph {
  @Field(() => ServiceGraph, { nullable: true })
  service?: ServiceGraph;

  @Field(() => BookingGraph, { nullable: true })
  booking?: BookingGraph;

  constructor(__: { service?: ServiceGraph; booking?: BookingGraph }) {
    if (__.service) this.service = __.service;
    if (__.booking) this.booking = __.booking;
  }
}
