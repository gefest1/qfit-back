import {
  Field,
  Float,
  GraphQLISODateTime,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { ClubGraph } from 'src/modules/club/models';
import { UserGraph } from 'src/modules/user/models/user.model';
import { Modify } from 'src/utils/modifyType';
import { SessionGraph } from './session.model';
import { Transaction } from './transaction.model';

export enum SessionStackStatus {
  credit = 'credit',
  success = 'success',
}

export interface SessionStack {
  _id: ObjectId;
  stackIds: ObjectId[];
  userId: ObjectId;
  clubId: ObjectId;
  generalPrice?: number;
  startTime: Date;
  endTime?: Date;
  transactions?: Transaction[];
  freeTraining?: boolean;
  bonus?: boolean;
  bonusAmount?: number;
  status?: keyof typeof SessionStackStatus;
}

registerEnumType(SessionStackStatus, {
  name: 'SessionStackStatus',
});

@ObjectType('SessionStack')
export class SessionStackGraph
  implements
    Modify<
      SessionStack,
      {
        _id: string;
        stackIds: string[];
        userId: string;
        clubId: string;
      }
    >
{
  @Field()
  _id: string;

  @Field(() => [String])
  stackIds: string[];

  @Field()
  userId: string;

  @Field()
  clubId: string;

  @Field(() => GraphQLISODateTime)
  startTime: Date;

  @Field(() => GraphQLISODateTime, { nullable: true })
  endTime?: Date;

  @Field(() => Float, { nullable: true })
  generalPrice?: number;

  @Field(() => [SessionGraph], { nullable: true })
  stack?: SessionGraph[];

  @Field(() => ClubGraph, { nullable: true })
  club?: ClubGraph;

  @Field(() => UserGraph, { nullable: true })
  user?: UserGraph;

  @Field({
    nullable: true,
  })
  freeTraining?: boolean;

  @Field({
    nullable: true,
  })
  bonus?: boolean;

  @Field({
    nullable: true,
  })
  bonusAmount: number;

  @Field(() => SessionStackStatus, { nullable: true })
  status: SessionStackStatus;

  @Field(() => Float, { nullable: true })
  debt?: number;

  constructor(
    __: Partial<SessionStack> & {
      stack?: SessionGraph[];
      club?: ClubGraph;
      user?: UserGraph;
      debt?: number;
    },
  ) {
    if (__._id != null) this._id = __._id.toHexString();
    if (__.stackIds != null)
      this.stackIds = __.stackIds.map((e) => e.toHexString());
    if (__.userId != null) this.userId = __.userId.toHexString();
    if (__.clubId != null) this.clubId = __.clubId.toHexString();
    if (__.generalPrice != null) this.generalPrice = __.generalPrice;
    if (__.startTime != null) this.startTime = __.startTime;
    if (__.endTime != null) this.endTime = __.endTime;
    if (__.stack != null) this.stack = __.stack;
    if (__.club != null) this.club = __.club;
    if (__.user != null) this.user = __.user;
    if (__.freeTraining != null) this.freeTraining = __.freeTraining;
    if (__.bonus != null) this.bonus = __.bonus;
    if (__.bonusAmount != null) this.bonusAmount = __.bonusAmount;
    if (__.status != null) this.status = SessionStackStatus[__.status];
    if (__.debt != null) this.debt = __.debt;
  }
}
