import {
  CanActivate,
  createParamDecorator,
  ExecutionContext,
  Injectable,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import * as jwt from 'jsonwebtoken';

import { ObjectId } from 'mongodb';
import { UserService } from '../user/services/user.service';
import { PublicKey } from 'src/utils/keys';
import { OwnerService } from 'src/owner/services/owner.services';

@Injectable()
export class PreAuthGuardGraph implements CanActivate {
  constructor(private userService: UserService) {}

  async canActivate(context: ExecutionContext) {
    const request = GqlExecutionContext.create(context).getContext().req;
    const { authorization } = request.headers;
    const graphqlRequestName =
      GqlExecutionContext.create(context).getInfo().path.key;
    if (
      graphqlRequestName === 'sendVerificationSMS' ||
      graphqlRequestName === 'checkSMSVerificationCode'
    )
      return true;
    if (!authorization) return false;
    try {
      const payload = jwt.verify(authorization, PublicKey) as {
        _id: string;
        dateAdded: string;
        email: string;
      };
      const _id = new ObjectId(payload._id);
      const user = await this.userService.findOne({
        user: { _id },
      });

      if (!user) return false;
      request.user = user;
      return true;
    } catch {
      return false;
    }
  }
}

@Injectable()
export class PreAuthAdminGuardGraph implements CanActivate {
  constructor(private userService: UserService) {}

  async canActivate(context: ExecutionContext) {
    const request = GqlExecutionContext.create(context).getContext().req;
    const { authorization } = request.headers;
    if (!authorization) return false;
    try {
      const payload = jwt.verify(authorization, PublicKey) as {
        _id: string;
        dateAdded: string;
        email: string;
      };
      const _id = new ObjectId(payload._id);
      const user = await this.userService.findOne({
        user: { _id },
      });

      if (!user || !user.isAdmin) return false;
      request.user = user;
      return true;
    } catch {
      return false;
    }
  }
}

@Injectable()
export class PreAuthOwnerGuardGraph implements CanActivate {
  constructor(private ownerService: OwnerService) {}

  async canActivate(context: ExecutionContext) {
    const request = GqlExecutionContext.create(context).getContext().req;
    const graphqlRequestName =
      GqlExecutionContext.create(context).getInfo().path.key;
    const { authorization } = request.headers;
    if (
      graphqlRequestName === 'registerOwner' ||
      graphqlRequestName === 'loginOwner'
    ) {
      return true;
    }
    if (!authorization) return false;
    try {
      const payload = jwt.verify(authorization, PublicKey) as {
        _id: string;
        dateAdded: string;
        email: string;
      };
      const _id = new ObjectId(payload._id);
      const user = await this.ownerService.findOne({
        owner: { _id },
      });
      if (!user || !user.ownedClubID) return false;
      request.user = user;
      return true;
    } catch {
      return false;
    }
  }
}

export const CurrentUserGraph = createParamDecorator(
  async (data: unknown, context: ExecutionContext) => {
    const request = GqlExecutionContext.create(context).getContext().req;
    return request.user;
  },
);
