import { Module } from '@nestjs/common';
import { OwnerModule } from 'src/owner/owner.module';

import { UserModule } from '../user/user.module';
import {
  PreAuthAdminGuardGraph,
  PreAuthGuardGraph,
  PreAuthOwnerGuardGraph,
} from './admin.guard';

@Module({
  imports: [UserModule, OwnerModule],
  providers: [
    PreAuthOwnerGuardGraph,
    PreAuthAdminGuardGraph,
    PreAuthGuardGraph,
  ],
  exports: [
    PreAuthOwnerGuardGraph,
    PreAuthAdminGuardGraph,
    PreAuthGuardGraph,
    UserModule,
    OwnerModule,
  ],
})
export class PreAuthModule {}
