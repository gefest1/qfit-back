import { Resolver } from '@nestjs/graphql';

import { ServiceService } from './services';

@Resolver()
export class ServiceResolver {
  constructor(public serviceService: ServiceService) {}
}
