import { ArgsType, Field } from '@nestjs/graphql';

import { Modify } from 'src/utils/modifyType';
import { Service } from './service.model';

@ArgsType()
export class EditService
  implements
    Modify<
      Partial<Service>,
      {
        _id: string;
        clubId?: string;
      }
    >
{
  @Field({ nullable: true })
  _id: string;

  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  price?: number;

  @Field({ nullable: true })
  startTime?: Date;

  @Field({ nullable: true })
  endTime?: Date;

  @Field({ nullable: true })
  type?: string;

  @Field({ nullable: true })
  clubId?: string;
}
