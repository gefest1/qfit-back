import { ArgsType, Field, Int, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { ClubGraph } from 'src/modules/club/models';
import { Modify } from 'src/utils/modifyType';

export interface Service {
  _id: ObjectId;
  name: string;
  price: number;
  capacity: number;
  startTime: Date;
  endTime: Date;
  type?: string;
  clubId: ObjectId;
}

type ServiceGraphAdditive = {
  club?: ClubGraph;
};

@ObjectType('Service')
export class ServiceGraph
  implements
    Modify<
      Service,
      {
        _id: string;
        clubId: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  name: string;

  @Field()
  price: number;

  @Field(() => Int)
  capacity: number;

  @Field()
  startTime: Date;

  @Field()
  endTime: Date;

  @Field({ nullable: true })
  type?: string;

  @Field()
  clubId: string;

  @Field({ nullable: true })
  club?: ClubGraph;

  constructor(__: Partial<Service> & ServiceGraphAdditive) {
    if (__._id != null) this._id = __._id.toHexString();
    if (__.name != null) this.name = __.name;
    if (__.price != null) this.price = __.price;
    if (__.startTime != null) this.startTime = __.startTime;
    if (__.endTime != null) this.endTime = __.endTime;
    if (__.type != null) this.type = __.type;
    if (__.clubId != null) this.clubId = __.clubId.toHexString();
    if (__.club) this.club = __.club;
    if (__.capacity) this.capacity = __.capacity;
  }
}

@ArgsType()
export class CreateService
  implements
    Omit<
      Modify<
        Service,
        {
          clubId: string;
        }
      >,
      '_id'
    >
{
  @Field()
  name: string;

  @Field()
  price: number;

  @Field()
  startTime: Date;

  @Field()
  endTime: Date;

  @Field(() => Int)
  capacity: number;

  @Field({ nullable: true })
  type?: string;

  @Field()
  clubId: string;
}
