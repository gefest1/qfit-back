import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { ApolloError } from 'apollo-server-express';
import { UseGuards } from '@nestjs/common';
import { PreAuthAdminGuardGraph } from 'src/modules/admin/admin.guard';
import { CreateService, Service, ServiceGraph } from '../models/service.model';
import { ServiceService } from '../services';
import { parseTime } from 'src/utils/parseTime';
import { EditService } from '../models/editService.model';
import { ObjectId } from 'mongodb';
import { filterObject } from 'src/utils/filterObject';
import { TypeService } from '../services/type.service';

@Resolver()
@UseGuards(PreAuthAdminGuardGraph)
export class AdminServiceResolver {
  constructor(
    public serviceService: ServiceService,
    public typeService: TypeService,
  ) {}

  @Mutation(() => ServiceGraph)
  async createServiceAdmin(
    @Args() args: CreateService,
  ): Promise<ServiceGraph | void> {
    const startTime = parseTime(args.startTime);
    const endTime = new Date(
      startTime.getTime() + args.endTime.getTime() - args.endTime.getTime(),
    );
    const _club = await this.serviceService.createService({
      ...args,
      startTime,
      endTime,
    });
    return new ServiceGraph(_club);
  }

  @Mutation(() => ServiceGraph)
  async editServiceAdmin(@Args() _args: EditService): Promise<ServiceGraph> {
    const { _id, clubId, ...args } = _args;
    if ((args.startTime && !args.endTime) || (!args.startTime && args.endTime))
      throw new ApolloError('startTime and endTime', '');
    const startTime = args.startTime && parseTime(args.startTime);
    const endTime =
      args.endTime &&
      new Date(
        startTime.getTime() + args.endTime.getTime() - args.startTime.getTime(),
      );

    const _newObj: Partial<Service> = filterObject(
      { ...args, endTime, startTime, clubId: clubId && new ObjectId(clubId) },
      ([_, value]) => {
        if (value === undefined) return false;
        return true;
      },
    );
    const returnObj = await this.serviceService.updateServiceById(
      new ObjectId(_id),
      _newObj,
    );
    return {
      ...returnObj,
      _id: returnObj._id.toHexString(),
      clubId: returnObj.clubId.toHexString(),
    };
  }

  @Mutation(() => [ServiceGraph])
  async editServiceTypesAdmin(
    @Args('oldType', { type: () => String }) oldType: string,
    @Args('clubId', { type: () => String }) clubId: string,
    @Args('newType', { type: () => String }) newType: string,
  ): Promise<ServiceGraph[]> {
    const _services = await this.typeService.updateTypes(
      new ObjectId(clubId),
      oldType,
      newType,
    );
    return _services.map((e) => {
      return {
        ...e,
        _id: e._id.toHexString(),
        clubId: e.clubId.toHexString(),
      };
    });
  }
}
