import { Args, Query, Resolver, GraphQLISODateTime } from '@nestjs/graphql';

import { UseGuards } from '@nestjs/common';
import { PreAuthGuardGraph } from 'src/modules/admin/admin.guard';

import { ServiceService } from '../services';
import { UserServiceService } from '../services/userService';
import { ServiceGraph } from '../models/service.model';

import { ClubGraph } from 'src/modules/club/models';

@Resolver()
@UseGuards(PreAuthGuardGraph)
export class UserServiceResolver {
  constructor(
    public serviceService: ServiceService,
    public userServiceService: UserServiceService,
  ) {}

  @Query(() => ServiceGraph, { nullable: true })
  async getCurrentDefaultServiceByDate(
    @Args('date', {
      type: () => GraphQLISODateTime,
    })
    date: Date,
    @Args('clubId', {
      type: () => String,
    })
    clubId: string,
  ): Promise<ServiceGraph | void> {
    const _services =
      await this.userServiceService.getCurrentDefaultServiceByDate({
        clubId,
        date,
      });

    return new ServiceGraph({
      ..._services,
      club: new ClubGraph(_services.club),
    });
  }

  @Query(() => [ServiceGraph])
  async getServicesOfAClub(
    @Args('clubId', {
      type: () => String,
    })
    clubId: string,
  ): Promise<ServiceGraph[]> {
    const _services = await this.userServiceService.getServicesOfClub({
      clubId,
    });

    return _services.map((_value) => new ServiceGraph(_value));
  }
}
