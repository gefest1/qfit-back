import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';

import { UseGuards } from '@nestjs/common';
import {
  CurrentUserGraph,
  PreAuthOwnerGuardGraph,
} from 'src/modules/admin/admin.guard';

import { ServiceService } from '../services';
import { CreateService, ServiceGraph } from '../models/service.model';
import { Owner } from 'src/owner/models/owner.interface';
import { paginate } from 'src/utils/paginate';
import { parseTime } from 'src/utils/parseTime';
import { ObjectId } from 'mongodb';

@Resolver()
@UseGuards(PreAuthOwnerGuardGraph)
export class OwnerServiceResolver {
  constructor(public serviceService: ServiceService) {}

  @Mutation(() => ServiceGraph)
  async createServiceOwner(
    @Args('name')
    name: string,
    @Args('price')
    price: number,
    @Args('startTime')
    _startTime: Date,
    @Args('endTime')
    _endTime: Date,
    @Args('capacity')
    capacity: number,
    @CurrentUserGraph() owner: Owner,
    @Args('type', { nullable: true })
    type?: string,
  ): Promise<ServiceGraph | void> {
    const startTime = parseTime(_startTime);
    const endTime = new Date(
      startTime.getTime() + _endTime.getTime() - _startTime.getTime(),
    );
    const _club = await this.serviceService.createService({
      name,
      price,
      capacity,
      type,
      startTime,
      endTime,
      clubId: owner.ownedClubID.toHexString(),
    });
    return new ServiceGraph(_club);
  }

  @Query(() => [ServiceGraph])
  async getServicesOwner(@CurrentUserGraph() owner: Owner) {
    const service = await this.serviceService.getServices({
      clubId: owner.ownedClubID,
    });
    const serviceResponce = service.map((val) => {
      const serviceGraph = new ServiceGraph({ ...val });
      return serviceGraph;
    });
    return serviceResponce;
  }

  @Mutation(() => String)
  async deleteServiceOwner(
    @Args('serviceId', { type: () => String }) serviceId: string,
    @CurrentUserGraph() owner: Owner,
  ) {
    await this.serviceService.deleteService({
      _id: new ObjectId(serviceId),
      clubId: owner.ownedClubID,
    });
    return 'success';
  }
}
