import { Inject, Injectable } from '@nestjs/common';
import { Db, Collection, ObjectId } from 'mongodb';
import { AWSPhotoUploadService } from 'src/modules/uploadPhoto/awsSpace.service';
import { Service } from '../models/service.model';

@Injectable()
export class TypeService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
    public photoUpload: AWSPhotoUploadService,
  ) {}

  get serviceCollection(): Collection<Service> {
    return this._mongodDb.collection('service');
  }

  async updateTypes(
    clubId: ObjectId,
    oldType: string,
    newType: string,
  ): Promise<Service[]> {
    await this.serviceCollection.updateMany(
      {
        type: oldType,
        clubId,
      },
      {
        $set: {
          type: newType,
        },
      },
    );

    return this.serviceCollection
      .find({
        type: newType,
        clubId,
      })
      .toArray();
  }
}
