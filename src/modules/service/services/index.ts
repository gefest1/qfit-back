import { Inject, Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { Db, Collection, ObjectId } from 'mongodb';
import { AWSPhotoUploadService } from 'src/modules/uploadPhoto/awsSpace.service';
import { CreateService, Service } from '../models/service.model';

@Injectable()
export class ServiceService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
    public photoUpload: AWSPhotoUploadService,
  ) {}

  get serviceCollection(): Collection<Service> {
    return this._mongodDb.collection('service');
  }

  async createService(arg: CreateService): Promise<Service> {
    const _service = {
      ...arg,
      clubId: new ObjectId(arg.clubId),
    };
    const { insertedId } = await this.serviceCollection.insertOne(_service);

    return {
      ..._service,
      _id: insertedId,
    };
  }

  async getServicesCursor(service: Partial<Service>) {
    const serviceResponce = this.serviceCollection.find(service);
    return serviceResponce;
  }

  async getServices(service: Partial<Service>) {
    const services = await this.serviceCollection.find(service).toArray();
    return services;
  }

  async updateServiceById(
    _id: ObjectId,
    edit: Partial<Service>,
  ): Promise<Service> {
    const result = await this.serviceCollection.findOneAndUpdate(
      {
        _id,
      },
      {
        $set: edit,
      },
      {
        returnDocument: 'after',
      },
    );
    return result.value;
  }

  async deleteService(args: Partial<Service>) {
    const deleteService = await this.serviceCollection.deleteOne(args);
    if (!deleteService.acknowledged)
      throw new ApolloError('the service was not deleted');
  }
}
