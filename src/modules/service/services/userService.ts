import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import { Club } from 'src/modules/club/models';

import { AWSPhotoUploadService } from 'src/modules/uploadPhoto/awsSpace.service';
import { getCorrectWeekDay } from 'src/utils/getCorrectWeekDay';
import { parseTime, weekTime } from 'src/utils/parseTime';
import { Service } from '../models/service.model';

type ServiceWithClub = Service & {
  club: Club;
};

@Injectable()
export class UserServiceService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
    public photoUpload: AWSPhotoUploadService,
  ) {}

  get serviceCollection(): Collection<Service> {
    return this._mongodDb.collection('service');
  }

  public async getServicesOfClub(__: { clubId: string }): Promise<Service[]> {
    return this.serviceCollection
      .find({
        clubId: new ObjectId(__.clubId),
      })
      .toArray();
  }

  async getCurrentDefaultServiceByDate(__: {
    date: Date;
    clubId: string;
  }): Promise<ServiceWithClub> {
    const { date: _date, clubId } = __;
    const date = parseTime(_date);
    if (getCorrectWeekDay(date.getUTCDay()) == 0) {
      const addNewDate = new Date(date.getTime() + weekTime);
      const service = await this.serviceCollection
        .aggregate<ServiceWithClub>([
          {
            $match: {
              $or: [
                {
                  clubId: new ObjectId(clubId),
                  trainingType: null,
                  startTime: {
                    $lte: addNewDate,
                  },
                  endTime: {
                    $gte: addNewDate,
                  },
                },
                {
                  clubId: new ObjectId(clubId),
                  trainingType: null,
                  startTime: {
                    $lte: date,
                  },
                  endTime: {
                    $gte: date,
                  },
                },
              ],
            },
          },
          {
            $lookup: {
              from: 'club',
              localField: 'clubId',
              foreignField: '_id',
              as: 'club',
            },
          },
          {
            $limit: 1,
          },
          { $unwind: '$club' },
        ])
        .toArray();
      if (!service?.length) return undefined;
      const _service = service[0];
      return _service;
    }

    const currentDefaultService = await this.serviceCollection
      .aggregate<ServiceWithClub>([
        {
          $match: {
            clubId: new ObjectId(clubId),
            type: null,
            startTime: {
              $lte: date,
            },
            endTime: {
              $gte: date,
            },
          },
        },
        {
          $lookup: {
            from: 'club',
            localField: 'clubId',
            foreignField: '_id',
            as: 'club',
          },
        },
        {
          $limit: 1,
        },
        { $unwind: '$club' },
      ])
      .toArray();
    if (!currentDefaultService?.length) return undefined;
    const _service = currentDefaultService[0];
    return _service;
  }

  async getServiceById(__: { id: string }): Promise<Service> {
    const { id } = __;
    return this.serviceCollection.findOne({
      _id: new ObjectId(id),
    });
  }
}
