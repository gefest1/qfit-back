import { Module } from '@nestjs/common';
import { OwnerModule } from 'src/owner/owner.module';
import { OwnerService } from 'src/owner/services/owner.services';
import { PreAuthOwnerGuardGraph } from '../admin/admin.guard';
import { PreAuthModule } from '../admin/admin.module';
import { AWSPhotoUploadModule } from '../uploadPhoto/awsSpace.module';
import { UserModule } from '../user/user.module';
import { AdminServiceResolver } from './resolvers/admin.resolver';
import { OwnerServiceResolver } from './resolvers/owner.resolver';
import { UserServiceResolver } from './resolvers/user.resolver';
import { ServiceResolver } from './service.resolver';
import { ServiceService } from './services';
import { TypeService } from './services/type.service';
import { UserServiceService } from './services/userService';

@Module({
  imports: [AWSPhotoUploadModule, UserModule, OwnerModule, PreAuthModule],
  providers: [
    ServiceService,
    OwnerServiceResolver,
    AdminServiceResolver,
    ServiceResolver,
    TypeService,
    UserServiceService,
    UserServiceResolver,
  ],
  exports: [OwnerServiceResolver, AdminServiceResolver, UserServiceService],
})
export class ServiceModule {}
