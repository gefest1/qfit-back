import { Injectable } from '@nestjs/common';
import { ObjectId } from 'mongodb';
import { User } from '../user/models/user.model';
import { Token } from './token.interface';
import * as jwt from 'jsonwebtoken';
import { PrivateKey } from 'src/utils/keys';
import { signOption } from './token.signOption';

@Injectable()
export class TokenService {
  create(args: {
    user: Pick<User, '_id' | 'phoneNumber' | 'dateAdded'> & { email?: string };
  }) {
    const { user } = args;
    const { _id, email, phoneNumber, dateAdded } = user;
    const id = _id.toHexString();
    const payload: Token = {
      _id: id,
      email,
      phoneNumber,
      dateAdded,
    };
    const token = jwt.sign(payload, PrivateKey, signOption);
    return token;
  }
}
