export interface Token {
  _id: string;
  email?: string;
  phoneNumber: string;
  dateAdded: Date;
}
