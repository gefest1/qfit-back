import { Module } from '@nestjs/common';
import { TokenService } from './token.service';

@Module({
  imports: [TokenService],
  providers: [TokenService],
  exports: [TokenService],
})
export class TokenModule {}
