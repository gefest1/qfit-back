import { ArgsType, Field, InputType, ObjectType } from '@nestjs/graphql';

export interface PhotoUrl {
  XL: string;
  M: string;
  thumbnail: string;
}

@ObjectType('PhotoUrl')
export class PhotoUrlGraph implements PhotoUrl {
  @Field()
  XL: string;

  @Field()
  M: string;

  @Field()
  thumbnail: string;
}

@InputType()
export class PhotoURLInput implements PhotoUrl {
  @Field()
  XL: string;

  @Field()
  M: string;

  @Field()
  thumbnail: string;
}
