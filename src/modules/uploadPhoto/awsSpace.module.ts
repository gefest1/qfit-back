import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AWSPhotoUploadService } from './awsSpace.service';

@Module({
  imports: [ConfigModule],
  providers: [AWSPhotoUploadService],
  exports: [AWSPhotoUploadService],
})
export class AWSPhotoUploadModule {}
