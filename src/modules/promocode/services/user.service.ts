import { Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { ObjectId } from 'mongodb';
import { UserService } from 'src/modules/user/services/user.service';
import { PromocodeTypes } from '../models/promocode.enum';
import { PromocodeService } from './promocode.service';

@Injectable()
export class UserPromocodeService {
  constructor(
    private userService: UserService,
    private promocodeService: PromocodeService,
  ) {}

  async usePromocode(args: { code: string; userId: ObjectId }) {
    const { code, userId } = args;
    const promocode = await this.promocodeService.findOne({ code });
    const { freeTrainings, bonus, type } = promocode;
    const promocodeAttachment = {
      [type]: freeTrainings ? freeTrainings : bonus,
    };
    const newCode = Math.random().toString(36).substring(5).toUpperCase();
    await Promise.all([
      this.userService.updateOne({
        findUser: { _id: userId },
        updateUser: promocodeAttachment,
        method: '$inc',
      }),
      this.promocodeService.updateOne({
        findPromocode: { code },
        updatePromocode: { code: newCode },
        method: '$set',
      }),
    ]);
    if (promocode.userId) {
      await this.userService.updateOne({
        findUser: { _id: promocode.userId },
        updateUser: promocodeAttachment,
        method: '$inc',
      });
    }
  }
}
