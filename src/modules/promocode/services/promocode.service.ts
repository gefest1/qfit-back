import { Inject, Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { Db } from 'mongodb';
import { Promocode } from '../models/promocode.interface';

@Injectable()
export class PromocodeService {
  constructor(@Inject('DATABASE_CONNECTION') private database: Db) {}

  get promocodeCollection() {
    const collection = this.database.collection('promocode');
    collection.createIndex({ code: 1 }, { unique: true });
    return this.database.collection('promocode');
  }

  async findOne(promocode: Partial<Promocode>): Promise<Promocode> {
    const promocodeResponce = await this.promocodeCollection.findOne<Promocode>(
      promocode,
    );
    if (!promocodeResponce) throw 'not promocode found';
    return promocodeResponce;
  }

  async updateOne(args: {
    findPromocode: Partial<Promocode>;
    updatePromocode: Partial<Promocode>;
    method: '$set' | '$inc' | '$addToSet' | '$push';
  }): Promise<void> {
    const { findPromocode, updatePromocode, method } = args;
    const updateQuery = { [method]: updatePromocode };
    this.promocodeCollection.updateOne(findPromocode, updateQuery);
  }
}
