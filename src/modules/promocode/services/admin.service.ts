import { Inject, Injectable } from '@nestjs/common';
import { Db, ObjectId } from 'mongodb';
import { Modify } from 'src/utils/modifyType';
import { CreatePromocode } from '../models/createPromocode.model';
import { Promocode } from '../models/promocode.interface';

@Injectable()
export class AdminPromocodeService {
  constructor(@Inject('DATABASE_CONNECTION') private database: Db) {}

  get promocodeCollection() {
    const collection = this.database.collection('promocode');
    collection.createIndex({ code: 1 }, { unique: true });
    return collection;
  }

  async createPromocode(args: CreatePromocode) {
    const { type, bonus, freeTrainings, userId: _userId } = args;
    const code = Math.random().toString(36).substring(5).toUpperCase();
    const currentDate = new Date();
    const userId = _userId && new ObjectId(_userId);
    console.log(userId);
    const promocode: Modify<
      Promocode,
      {
        _id?: ObjectId;
      }
    > = {
      dateAdded: currentDate,
      bonus,
      freeTrainings,
      userId,
      type,
      code,
    };
    const insertPromocode = await this.promocodeCollection.insertOne(
      promocode,
      {
        ignoreUndefined: true,
      },
    );
    promocode._id = insertPromocode.insertedId;
    return promocode;
  }
}
