import { Module } from '@nestjs/common';
import { PreAuthModule } from '../admin/admin.module';
import { AdminPromocodeResolver } from './resolvers/admin.resolver';
import { UserPromocodeResolver } from './resolvers/user.resolver';
import { AdminPromocodeService } from './services/admin.service';
import { PromocodeService } from './services/promocode.service';
import { UserPromocodeService } from './services/user.service';

@Module({
  imports: [PreAuthModule],
  providers: [
    AdminPromocodeService,
    AdminPromocodeResolver,
    UserPromocodeService,
    UserPromocodeResolver,
    PromocodeService,
  ],
  exports: [AdminPromocodeResolver],
})
export class PromocodeModule {}
