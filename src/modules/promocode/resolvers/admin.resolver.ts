import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { PreAuthAdminGuardGraph } from 'src/modules/admin/admin.guard';
import { CreatePromocode } from '../models/createPromocode.model';
import { PromocodeGraph } from '../models/promocode.model';
import { AdminPromocodeService } from '../services/admin.service';

@Resolver()
@UseGuards(PreAuthAdminGuardGraph)
export class AdminPromocodeResolver {
  constructor(private adminPromocodeService: AdminPromocodeService) {}

  @Mutation(() => PromocodeGraph)
  async createPromocodeAdmin(
    @Args() args: CreatePromocode,
  ): Promise<PromocodeGraph> {
    const insertPromocode = await this.adminPromocodeService.createPromocode(
      args,
    );
    const promocodeResponce = new PromocodeGraph({ ...insertPromocode });
    return promocodeResponce;
  }
}
