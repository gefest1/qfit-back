import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { ApolloError } from 'apollo-server-express';
import {
  CurrentUserGraph,
  PreAuthGuardGraph,
} from 'src/modules/admin/admin.guard';
import { User } from 'src/modules/user/models/user.model';
import { CreatePromocode } from '../models/createPromocode.model';
import { PromocodeGraph } from '../models/promocode.model';
import { AdminPromocodeService } from '../services/admin.service';
import { PromocodeService } from '../services/promocode.service';
import { UserPromocodeService } from '../services/user.service';

@Resolver()
@UseGuards(PreAuthGuardGraph)
export class UserPromocodeResolver {
  constructor(
    private promocodeService: PromocodeService,
    private userPromocodeService: UserPromocodeService,
    private adminPromocodeService: AdminPromocodeService,
  ) {}

  @Mutation(() => String)
  async usePromocodeUser(
    @CurrentUserGraph() user: User,
    @Args('code', { type: () => String }) code: string,
  ) {
    try {
      await this.userPromocodeService.usePromocode({ code, userId: user._id });
    } catch (e) {
      throw new ApolloError(`${e}`);
    }
    return 'success';
  }

  @Mutation(() => PromocodeGraph)
  async createPromocodeUser(
    @Args() args: CreatePromocode,
    @CurrentUserGraph() user: User,
  ): Promise<PromocodeGraph> {
    const insertPromocode = await this.adminPromocodeService.createPromocode({
      ...args,
      userId: user._id.toHexString(),
    });
    const promocodeResponce = new PromocodeGraph({ ...insertPromocode });
    return promocodeResponce;
  }
}
