import { registerEnumType } from '@nestjs/graphql';

export enum PromocodeTypes {
  freeTrainings = 'freeTrainings',
  bonus = 'bonus',
}

registerEnumType(PromocodeTypes, {
  name: 'PromocodeTypes',
});
