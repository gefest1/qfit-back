import { ObjectId } from 'mongodb';
import { PromocodeTypes } from './promocode.enum';

export interface Promocode {
  _id: ObjectId;
  type: PromocodeTypes;
  dateAdded: Date;
  bonus?: number;
  freeTrainings?: number;
  userId?: ObjectId;
  code: string;
}
