import { ArgsType, Field } from '@nestjs/graphql';
import { Modify } from 'src/utils/modifyType';
import { PromocodeTypes } from './promocode.enum';
import { Promocode } from './promocode.interface';

@ArgsType()
export class CreatePromocode
  implements
    Modify<
      Omit<Promocode, '_id' | 'dateAdded' | 'code'>,
      {
        userId?: string;
      }
    >
{
  @Field(() => PromocodeTypes)
  type: PromocodeTypes;

  @Field({ nullable: true })
  bonus?: number;

  @Field({ nullable: true })
  freeTrainings?: number;

  @Field({ nullable: true })
  userId?: string;
}
