import { Field, ObjectType } from '@nestjs/graphql';
import { Modify } from 'src/utils/modifyType';
import { PromocodeTypes } from './promocode.enum';
import { Promocode } from './promocode.interface';

@ObjectType('Promocode')
export class PromocodeGraph
  implements
    Modify<
      Promocode,
      {
        _id: string;
        userId?: string;
        type: PromocodeTypes;
      }
    >
{
  @Field()
  _id: string;

  @Field(() => PromocodeTypes, { nullable: false })
  type: PromocodeTypes;

  @Field(() => Date)
  dateAdded: Date;

  @Field({ nullable: true })
  bonus?: number;

  @Field({ nullable: true })
  freeTrainings?: number;

  @Field({ nullable: true })
  userId?: string;

  @Field()
  code: string;

  constructor(promocode: Partial<Promocode>) {
    if (promocode._id) this._id = promocode._id.toHexString();
    if (promocode.type) this.type = promocode.type;
    if (promocode.dateAdded) this.dateAdded = promocode.dateAdded;
    if (promocode.bonus) this.bonus = promocode.bonus;
    if (promocode.freeTrainings) this.freeTrainings = promocode.freeTrainings;
    if (promocode.userId) this.userId = promocode.userId.toHexString();
    if (promocode.code) this.code = promocode.code;
  }
}
