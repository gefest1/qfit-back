import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { EmailService } from '../services/email.service';
import { createTransport } from 'nodemailer';
import { EmailGraph } from '../models/email.model';
import {
  CurrentUserGraph,
  PreAuthGuardGraph,
} from 'src/modules/admin/admin.guard';
import { User } from 'src/modules/user/models/user.model';

@Resolver()
export class EmailResolver {
  constructor(private emailService: EmailService) {}

  @Mutation(() => EmailGraph)
  @UseGuards(PreAuthGuardGraph)
  async support(
    @Args('text') text: string,
    @Args('subject') subject: string,
    @CurrentUserGraph() user: User,
  ): Promise<EmailGraph> {
    const mail = createTransport({
      service: 'gmail',
      auth: {
        user: 'Qrfit.info@gmail.com',
        pass: 'qfit1973',
      },
    });

    const mailOptions = {
      from: 'Qrfit.info@gmail.com',
      to: 'Qrfit.info@gmail.com',
      subject: subject,
      text: `Id ${user._id}\nFullName ${user.fullName}\nPhoneNumber ${user.phoneNumber}\n\n${text}`,
    };

    await mail.sendMail(mailOptions);

    return new EmailGraph({
      subject: subject,
      text: text,
    });
  }
}
