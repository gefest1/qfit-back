import { Field, InputType, ObjectType } from '@nestjs/graphql';

export interface Email {
  subject: string;
  text: string;
}

@ObjectType('Email')
export class EmailGraph {
  @Field()
  subject: string;

  @Field()
  text: string;

  constructor(email: Partial<Email>) {
    if (email.subject) this.subject = email.subject;
    if (email.text) this.text = email.text;
  }
}
