import { Module } from '@nestjs/common';
import { PreAuthModule } from '../admin/admin.module';
import { EmailResolver } from './resolvers/email.resolver';
import { EmailService } from './services/email.service';

@Module({
  imports: [PreAuthModule],
  providers: [EmailService, EmailResolver],
  exports: [EmailService],
})
export class EmailModule {}
