import { Inject, Injectable } from '@nestjs/common';
import { ApolloError } from 'apollo-server-express';
import { AggregationCursor, Collection, Db, ObjectId } from 'mongodb';
import { Booking } from '../models/booking.model';

@Injectable()
export class OwnerBookingService {
  constructor(@Inject('DATABASE_CONNECTION') private _mongodb: Db) {}

  get bookingCollection(): Collection<Booking> {
    return this._mongodb.collection('booking');
  }

  async deleteBooking(args: { booking: Partial<Booking> }): Promise<void> {
    const { booking } = args;
    const deleteBooking = await this.bookingCollection.deleteOne(booking);
    if (deleteBooking.deletedCount === 0)
      throw new ApolloError('nothing was deleted');
  }

  async getUpcomingBookingsOfAClub(args: {
    findFields: (keyof Booking)[];
    findValues: any[];
    clubId: ObjectId;
  }) {
    const { clubId, findFields, findValues } = args;
    const query: any = {};
    findFields.map((val, ind) => {
      query[val] = findValues[ind];
    });
    const bookings = await this.bookingCollection
      .aggregate([
        { $match: query },
        {
          $lookup: {
            from: 'service',
            let: {
              serviceId: '$serviceId',
            },
            pipeline: [
              {
                $match: {
                  $expr: {
                    $and: [
                      { $eq: ['$_id', '$$serviceId'] },
                      { $eq: ['$clubId', clubId] },
                    ],
                  },
                },
              },
            ],
            as: 'service',
          },
        },
        {
          $lookup: {
            from: 'user',
            localField: 'userId',
            foreignField: '_id',
            as: 'user',
          },
        },
        {
          $unwind: '$service',
        },
        {
          $project: {
            _id: 1,
            user: {
              $arrayElemAt: ['$user', 0],
            },
            service: 1,
            date: 1,
            createDate: 1,
            stackId: 1,
          },
        },
      ])
      .toArray();
    return bookings;
  }

  async getUpcomingBookingsOfAClubCursor(args: {
    findFields: (keyof Booking)[];
    findValues: any[];
    clubId: ObjectId;
  }): Promise<AggregationCursor<Booking>> {
    const { clubId, findFields, findValues } = args;
    const query: any = {};
    findFields.map((val, ind) => {
      query[val] = findValues[ind];
    });
    const bookingCursor = this.bookingCollection.aggregate([
      { $match: query },
      {
        $lookup: {
          from: 'service',
          let: {
            serviceId: '$serviceId',
          },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [
                    { $eq: ['$_id', '$$serviceId'] },
                    { $eq: ['$clubId', clubId] },
                  ],
                },
              },
            },
          ],
          as: 'service',
        },
      },
      {
        $lookup: {
          from: 'user',
          localField: 'userId',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
        $unwind: '$service',
      },
      {
        $project: {
          _id: 1,
          user: {
            $arrayElemAt: ['$user', 0],
          },
          service: 1,
          date: 1,
          createDate: 1,
          stackId: 1,
        },
      },
    ]);
    return bookingCursor;
  }
}
