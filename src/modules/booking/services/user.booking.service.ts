import { Inject, Injectable } from '@nestjs/common';
import * as moment from 'moment';
import { Db, Collection, ObjectId } from 'mongodb';
import { Club } from 'src/modules/club/models';
import { Service } from 'src/modules/service/models/service.model';
import { Booking } from '../models/booking.model';

type BookingWtihServiceClubType = Booking & {
  service: Service;
  club: Club;
};

@Injectable()
export class UserBookingService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
  ) {}

  get bookingCollection(): Collection<Booking> {
    return this._mongodDb.collection('booking');
  }

  async createBooking(__: {
    serviceId: ObjectId;
    date: Date;
    userId: ObjectId;
  }): Promise<Booking> {
    const { serviceId, date, userId } = __;
    const _newObj = {
      serviceId,
      date,
      createDate: new Date(),
      userId,
    };
    const _insert = await this.bookingCollection.insertOne(_newObj);

    return {
      ..._newObj,
      _id: _insert.insertedId,
    };
  }

  async getClosestBooking(__: {
    userId: ObjectId;
    date: Date;
  }): Promise<BookingWtihServiceClubType | void> {
    const { userId, date } = __;
    const _book = await this.bookingCollection
      .aggregate<BookingWtihServiceClubType>([
        {
          $match: {
            userId,
            date: {
              $lt: moment(date).add(30, 'm').toDate(),
              $gte: moment(date).subtract(30, 'm').toDate(),
            },
          },
        },
        {
          $lookup: {
            from: 'service',
            localField: 'serviceId',
            foreignField: '_id',
            as: 'service',
          },
        },

        {
          $lookup: {
            from: 'club',
            localField: 'service.clubId',
            foreignField: '_id',
            as: 'club',
          },
        },
        {
          $limit: 1,
        },
        { $unwind: '$service' },
        { $unwind: '$club' },
      ])
      .toArray();
    if (!_book || !_book.length) return;
    return _book[0];
  }

  async getBookingWithClubAndService(__: {
    bookingId: string;
  }): Promise<BookingWtihServiceClubType> {
    const { bookingId } = __;
    const _book = await this.bookingCollection
      .aggregate<BookingWtihServiceClubType>([
        {
          $match: {
            _id: new ObjectId(bookingId),
          },
        },
        {
          $lookup: {
            from: 'service',
            localField: 'serviceId',
            foreignField: '_id',
            as: 'service',
          },
        },

        {
          $lookup: {
            from: 'club',
            localField: 'service.clubId',
            foreignField: '_id',
            as: 'club',
          },
        },
        {
          $limit: 1,
        },
        { $unwind: '$service' },
        { $unwind: '$club' },
      ])
      .toArray();
    return _book[0];
  }

  async getFutureBooking(__: {
    userId: ObjectId;
  }): Promise<BookingWtihServiceClubType[]> {
    const bookings = await this.bookingCollection
      .aggregate<BookingWtihServiceClubType>([
        { $match: { userId: __.userId, stackId: null } },
        {
          $lookup: {
            from: 'service',
            localField: 'serviceId',
            foreignField: '_id',
            as: 'service',
          },
        },
        {
          $lookup: {
            from: 'club',
            localField: 'service.clubId',
            foreignField: '_id',
            as: 'club',
          },
        },
        { $unwind: '$service' },
        { $unwind: '$club' },
      ])
      .toArray();
    return bookings;
  }

  async updateBooking(__: { _id: ObjectId; stackId: ObjectId }) {
    const { _id, stackId } = __;
    this.bookingCollection.findOneAndUpdate(
      {
        _id,
      },
      {
        $set: {
          stackId,
        },
      },
      {
        returnDocument: 'after',
      },
    );
  }
}
