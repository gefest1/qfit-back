import { UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import {
  CurrentUserGraph,
  PreAuthOwnerGuardGraph,
} from 'src/modules/admin/admin.guard';
import { ClubGraph } from 'src/modules/club/models';
import { Service } from 'src/modules/service/models/service.model';
import { CreditCardGraph } from 'src/modules/user/models/creditCard.model';
import { User } from 'src/modules/user/models/user.model';
import { Owner } from 'src/owner/models/owner.interface';
import { paginate } from 'src/utils/paginate';
import { Booking, BookingGraph } from '../models/booking.model';
import { OwnerBookingService } from '../services/owner.booking.service';

@Resolver()
@UseGuards(PreAuthOwnerGuardGraph)
export class OwnerBookingResolver {
  constructor(private ownerBookingService: OwnerBookingService) {}

  @Query(() => [BookingGraph])
  async getUpcomingBookingsOwner(@CurrentUserGraph() user: Owner) {
    const currentDate = new Date();
    const booking = await this.ownerBookingService.getUpcomingBookingsOfAClub({
      findFields: ['date'],
      findValues: [{ $gt: currentDate }],
      clubId: user.ownedClubID,
    });
    const bookingFiltered = booking.map(
      (val: Booking & { service: Service; user: User }) => {
        const { creditCard, invitedBy, youInvited } = val.user;
        return {
          ...val,
          service: {
            ...val.service,
            _id: val.service._id.toHexString(),
            clubId: val.service.clubId.toHexString(),
          },
          user: {
            ...val.user,
            _id: val.user._id.toHexString(),
            invitedBy: invitedBy ? invitedBy.toHexString() : null,
            youInvited: youInvited
              ? youInvited.map((val) => val.toHexString())
              : null,
            creditCard: creditCard
              ? creditCard.map((val) => new CreditCardGraph({ ...val }))
              : null,
          },
          club: null,
        };
      },
    );
    const bookingResponce = bookingFiltered.map((val) => {
      const bookingGraph = new BookingGraph({ ...val });
      return bookingGraph;
    });
    return bookingResponce;
  }

  @Mutation(() => String)
  async cancelBookingOwner(
    @Args('bookingId', { type: () => String }) bookingId: string,
  ) {
    const bookingID = new ObjectId(bookingId);
    await this.ownerBookingService.deleteBooking({
      booking: { _id: bookingID },
    });
    return 'success';
  }
}
