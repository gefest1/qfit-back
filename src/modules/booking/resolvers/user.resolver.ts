import { UseGuards } from '@nestjs/common';
import {
  Args,
  Resolver,
  GraphQLISODateTime,
  Query,
  Mutation,
} from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import {
  CurrentUserGraph,
  PreAuthGuardGraph,
} from 'src/modules/admin/admin.guard';
import { ClubGraph } from 'src/modules/club/models';
import { ServiceGraph } from 'src/modules/service/models/service.model';
import { CreditCardGraph } from 'src/modules/user/models/creditCard.model';
import { User } from 'src/modules/user/models/user.model';
import { BookingGraph } from '../models/booking.model';
import { UserBookingService } from '../services/user.booking.service';

@Resolver()
@UseGuards(PreAuthGuardGraph)
export class BookingUserServiceResolver {
  constructor(public bookingService: UserBookingService) {}

  @Query(() => BookingGraph, {
    nullable: true,
  })
  async getClosestBooking(
    @CurrentUserGraph() user: User,
  ): Promise<BookingGraph | void> {
    const closestBooking = await this.bookingService.getClosestBooking({
      date: new Date(),
      userId: new ObjectId(user._id),
    });
    return (
      closestBooking &&
      new BookingGraph({
        ...closestBooking,
        service:
          closestBooking.service && new ServiceGraph(closestBooking.service),
        club: closestBooking.club && new ClubGraph(closestBooking.club),
      })
    );
  }

  @Mutation(() => BookingGraph)
  async createBooking(
    @CurrentUserGraph() user: User,
    @Args('date', { type: () => GraphQLISODateTime }) date: Date,
    @Args('serviceId') serviceId: string,
  ): Promise<BookingGraph> {
    const _booking = await this.bookingService.createBooking({
      date,
      serviceId: new ObjectId(serviceId),
      userId: user._id,
    });
    return new BookingGraph(_booking);
  }

  @Query(() => [BookingGraph])
  async getUpcomingBookingsOfUser(
    @CurrentUserGraph() user: User,
  ): Promise<BookingGraph[]> {
    const bookings = await this.bookingService.getFutureBooking({
      userId: user._id,
    });
    return bookings.map(
      (_val) =>
        new BookingGraph({
          ..._val,
          club: _val.club && new ClubGraph(_val.club),
          service: _val.service && new ServiceGraph(_val.service),
        }),
    );
  }

  @Query(() => [CreditCardGraph])
  async getCreditCards(
    @CurrentUserGraph() user: User,
  ): Promise<CreditCardGraph[]> {
    return (user.creditCard ?? []).map((e) => new CreditCardGraph(e));
  }
}
