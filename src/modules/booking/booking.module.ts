import { Module } from '@nestjs/common';
import { PreAuthModule } from '../admin/admin.module';
import { UserModule } from '../user/user.module';
import { OwnerBookingResolver } from './resolvers/owner.resolver';
import { BookingUserServiceResolver } from './resolvers/user.resolver';
import { OwnerBookingService } from './services/owner.booking.service';
import { UserBookingService } from './services/user.booking.service';

@Module({
  imports: [UserModule, PreAuthModule],
  providers: [
    BookingUserServiceResolver,
    UserBookingService,
    OwnerBookingService,
    OwnerBookingResolver,
  ],
  exports: [UserBookingService],
})
export class BookingModule {}
