import { Field, ObjectType, GraphQLISODateTime } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { ClubGraph } from 'src/modules/club/models';
import { ServiceGraph } from 'src/modules/service/models/service.model';
import { UserGraph } from 'src/modules/user/models/user.model';
import { Modify } from 'src/utils/modifyType';

export interface Booking {
  _id: ObjectId;
  serviceId: ObjectId;
  date: Date;
  createDate: Date;
  userId: ObjectId;
  stackId?: ObjectId;
}

type BookingAdditive = {
  service?: ServiceGraph;
  club?: ClubGraph;
  user?: UserGraph;
};

@ObjectType('Booking')
export class BookingGraph
  implements
    Modify<
      Booking,
      {
        _id: string;
        serviceId: string;
        userId: string;
        stackId?: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  serviceId: string;

  @Field(() => GraphQLISODateTime)
  date: Date;

  @Field(() => GraphQLISODateTime)
  createDate: Date;

  @Field()
  userId: string;

  @Field({ nullable: true })
  stackId?: string;

  @Field(() => ServiceGraph, { nullable: true })
  service?: ServiceGraph;

  @Field(() => ClubGraph, { nullable: true })
  club?: ClubGraph;

  @Field(() => UserGraph, { nullable: true })
  user?: UserGraph;

  constructor(__: Partial<Booking> & BookingAdditive) {
    if (__._id) this._id = __._id.toHexString();
    if (__.serviceId) this.serviceId = __.serviceId.toHexString();
    if (__.date) this.date = __.date;
    if (__.createDate) this.createDate = __.createDate;
    if (__.userId) this.userId = __.userId.toHexString();
    if (__.stackId) this.stackId = __.stackId.toHexString();
    if (__.club) this.club = __.club;
    if (__.service) this.service = __.service;
    if (__.user) this.user = __.user;
  }
}
