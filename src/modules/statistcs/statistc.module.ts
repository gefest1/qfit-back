import { Module } from '@nestjs/common';
import { PreAuthModule } from '../admin/admin.module';
import { StatisticResolver } from './resolvers/statistc.resolver';
import { StatisticService } from './services/statistc.service';

@Module({
  imports: [PreAuthModule],
  providers: [StatisticService, StatisticResolver],
  exports: [],
})
export class StatistcModule {}
