import { Inject, Injectable } from '@nestjs/common';
import { Db, Collection } from 'mongodb';

import { Statistic } from '../models/statistic.model';

@Injectable()
export class StatisticService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
  ) {}

  get statisticCollection(): Collection<Statistic> {
    return this._mongodDb.collection('statistic');
  }
  async createStatistic(__: Statistic) {
    this.statisticCollection.insertOne(__);
  }
}
