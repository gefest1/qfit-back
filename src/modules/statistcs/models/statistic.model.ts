import { ObjectId } from 'mongodb';

export interface Statistic {
  trainingType: string;
  personInfo: string;
  dayTimeInfo: string;
  cityName: string;
  userId: ObjectId;
  age: number;
}
