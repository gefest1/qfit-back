import { UseGuards } from '@nestjs/common';
import { Args, Int, Mutation, Resolver } from '@nestjs/graphql';
import {
  CurrentUserGraph,
  PreAuthGuardGraph,
} from 'src/modules/admin/admin.guard';
import { User } from 'src/modules/user/models/user.model';
import { StatisticService } from '../services/statistc.service';

@Resolver()
@UseGuards(PreAuthGuardGraph)
export class StatisticResolver {
  constructor(public statisticService: StatisticService) {}

  @Mutation(() => String)
  async addStatistic(
    @Args('trainingType')
    trainingType: string,
    @Args('personInfo')
    personInfo: string,
    @Args('dayTimeInfo')
    dayTimeInfo: string,
    @Args('cityName')
    cityName: string,
    @Args('age', {
      type: () => Int,
    })
    age: number,
    @CurrentUserGraph()
    user: User,
  ): Promise<string> {
    await this.statisticService.createStatistic({
      trainingType,
      personInfo,
      dayTimeInfo,
      cityName,
      age,
      userId: user._id,
    });
    return 'SUCCESS';
  }
}
