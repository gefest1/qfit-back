import { ArgsType, Field, ObjectType } from '@nestjs/graphql';
import { ObjectId } from 'mongodb';
import { Modify } from 'src/utils/modifyType';

export interface CreditCard {
  _id: ObjectId;
  cardType: string;
  last4Digits: string;
  cardToken: string;
  cardHolderName: string;
}

@ObjectType('CreditCard')
export class CreditCardGraph
  implements
    Modify<
      CreditCard,
      {
        _id: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  cardType: string;

  @Field()
  last4Digits: string;

  cardToken: string;

  @Field()
  cardHolderName: string;

  constructor(__: Partial<CreditCard>) {
    if (__._id) this._id = __._id.toHexString();
    if (__.cardType) this.cardType = __.cardType;
    if (__.last4Digits) this.last4Digits = __.last4Digits;
    if (__.cardToken) this.cardToken = __.cardToken;
    if (__.cardHolderName) this.cardHolderName = __.cardHolderName;
  }
}

@ArgsType()
export class CreditCardInput implements Omit<CreditCard, '_id'> {
  @Field()
  cardType: string;

  @Field()
  last4Digits: string;

  @Field()
  cardToken: string;

  @Field()
  cardHolderName: string;
}
