import { registerEnumType } from '@nestjs/graphql';

export enum AllowedUserSex {
  Male = 'Male',
  Female = 'Female',
  Undefined = 'Undefined',
}

registerEnumType(AllowedUserSex, {
  name: 'AllowedUserSex',
});
