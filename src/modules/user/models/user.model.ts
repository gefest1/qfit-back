import {
  ArgsType,
  Field,
  GraphQLISODateTime,
  ObjectType,
} from '@nestjs/graphql';
import { Modify } from 'src/utils/modifyType';
import { ObjectId } from 'mongodb';
import { AllowedUserSex } from './user.enum';
import {
  PhotoUrl,
  PhotoUrlGraph,
} from 'src/modules/uploadPhoto/models/photo.model';
import {
  CreditCard,
  CreditCardGraph,
  CreditCardInput,
} from './creditCard.model';
import { FileUpload, GraphQLUpload } from 'graphql-upload';

export interface User {
  _id: ObjectId;
  fullName: string;
  phoneNumber: string;
  email?: string;
  dateOfBirth?: Date | null;
  dateAdded?: Date;
  sex?: AllowedUserSex;
  photoURL?: PhotoUrl;
  token?: string;
  invitedBy?: ObjectId;
  youInvited?: ObjectId[];
  isAdmin?: boolean;
  creditCard?: CreditCard[];
  bonus?: number;
  ownedClubID?: ObjectId;
  freeTrainings?: number;
}

@ObjectType('User')
export class UserGraph
  implements
    Modify<
      User,
      {
        _id: string;
        invitedBy?: string;
        youInvited?: string[];
        phoneNumber?: string;
        email?: string;
        fullName?: string;
        creditCard?: CreditCardGraph[];
      }
    >
{
  @Field()
  _id: string;

  @Field({ nullable: true })
  fullName?: string;

  @Field({ nullable: true })
  phoneNumber?: string;

  @Field({ nullable: true })
  email?: string;

  @Field(() => GraphQLISODateTime, { nullable: true })
  dateOfBirth?: Date;

  @Field(() => GraphQLISODateTime, { nullable: true })
  dateAdded?: Date;

  @Field(() => AllowedUserSex, { nullable: true })
  sex?: AllowedUserSex;

  @Field(() => PhotoUrlGraph, { nullable: true })
  photoURL?: PhotoUrlGraph;

  @Field({ nullable: true })
  token?: string;

  @Field({ nullable: true })
  invitedBy?: string;

  @Field(() => [String], { nullable: true })
  youInvited?: string[];

  @Field({ nullable: true })
  isAdmin?: boolean;

  @Field(() => [CreditCardGraph], { nullable: true })
  creditCard?: CreditCardGraph[];

  @Field({ nullable: true })
  bonus?: number;

  @Field({ nullable: true })
  freeTrainings?: number;

  constructor(
    user: Modify<
      Partial<User>,
      {
        creditCard?: CreditCardGraph[];
      }
    >,
  ) {
    if (user._id != null) this._id = user._id.toHexString();
    if (user.fullName != null) this.fullName = user.fullName;
    if (user.phoneNumber != null) this.phoneNumber = user.phoneNumber;
    if (user.email != null) this.email = user.email;
    if (user.dateOfBirth != null) this.dateOfBirth = user.dateOfBirth;
    if (user.dateAdded != null) this.dateAdded = user.dateAdded;
    if (user.sex != null) this.sex = user.sex;
    if (user.photoURL != null) this.photoURL = user.photoURL;
    if (user.token != null) this.token = user.token;
    if (user.invitedBy != null) this.invitedBy = user.invitedBy.toHexString();
    if (user.youInvited != null)
      this.youInvited = user.youInvited.map((val) => val.toHexString());
    if (user.isAdmin != null) this.isAdmin = user.isAdmin;
    if (user.creditCard != null) this.creditCard = user.creditCard;
    if (user.bonus != null) this.bonus = user.bonus;
    if (user.freeTrainings != null) this.freeTrainings = user.freeTrainings;
  }
}

@ArgsType()
export class CreateUser
  implements
    Modify<
      Partial<User>,
      {
        creditCard?: CreditCardInput[];
      }
    >
{
  @Field()
  fullName: string;

  @Field()
  phoneNumber: string;

  @Field()
  email: string;

  @Field(() => GraphQLISODateTime, { nullable: true })
  dateOfBirth?: Date;

  @Field(() => AllowedUserSex, { nullable: true })
  sex: AllowedUserSex;

  @Field({ nullable: true })
  promocode: string;

  // @Field(() => [CreditCardInput], { nullable: true })
  // creditCard?: CreditCardInput[];

  @Field(() => GraphQLUpload, {
    name: 'avatar',
    nullable: true,
  })
  avatar: Promise<FileUpload>;
}
