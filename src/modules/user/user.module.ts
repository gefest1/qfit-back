import { HttpModule } from '@nestjs/axios';
import { CacheModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SMSModule } from 'src/utils/SMS/SMS.service';
import { UserResolver } from './resolvers/user.resolver';
import { UserService } from './services/user.service';
import { AWSPhotoUploadModule } from '../uploadPhoto/awsSpace.module';
import { TokenModule } from '../token/token.module';
@Module({
  imports: [
    SMSModule,
    HttpModule,
    ConfigModule,
    CacheModule.register(),
    AWSPhotoUploadModule,
    TokenModule,
  ],
  providers: [UserResolver, UserService],
  exports: [UserService],
})
export class UserModule {}
