import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import { CreditCard } from '../models/creditCard.model';
import { User } from '../models/user.model';

@Injectable()
export class SavePaymentService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
  ) {}

  get userCollection(): Collection<User> {
    return this._mongodDb.collection('user');
  }

  async saveCard(__: {
    userId: ObjectId;
    cardHolderName: string;
    cardToken: string;
    cardType: string;
    last4Digits: string;
  }): Promise<CreditCard> {
    const { userId, cardHolderName, cardToken, cardType, last4Digits } = __;
    const _val = {
      _id: new ObjectId(),
      cardHolderName,
      cardToken,
      cardType,
      last4Digits,
    };
    const updated = await this.userCollection.updateOne(
      {
        _id: userId,
      },
      {
        $addToSet: {
          creditCard: _val,
        },
      },
    );
    if (!updated.modifiedCount) throw 'something went wrong';

    return _val;
  }
}
