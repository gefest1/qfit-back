import { Inject, Injectable } from '@nestjs/common';
import { Db, FindCursor, ObjectId } from 'mongodb';
import { AdminPromocodeService } from 'src/modules/promocode/services/admin.service';
import { PromocodeService } from 'src/modules/promocode/services/promocode.service';
import { Modify } from 'src/utils/modifyType';
import { AWSPhotoUploadService } from '../../uploadPhoto/awsSpace.service';

import { CreateUser, User } from '../models/user.model';

@Injectable()
export class UserService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    private db: Db,
    public photoUpload: AWSPhotoUploadService,
  ) {
    this.db
      .collection('user')
      .createIndex({ phoneNumber: 1 }, { unique: true });
  }

  get getCollection() {
    return this.db.collection<User>('user');
  }

  async findOne(args: { user: Partial<User> }): Promise<User> {
    const { user } = args;
    const userResponce: User = (await this.getCollection.findOne(user)) as User;
    return userResponce;
  }

  async findMany(args: { users: Partial<User> }): Promise<User[]> {
    const { users } = args;
    const usersResponce: User[] = await this.db
      .collection<User>('user')
      .find(users)
      .toArray();
    return usersResponce;
  }

  findManyCursor(args: { user: User }): FindCursor<User> {
    const { user } = args;
    const usersCursor = this.db.collection<User>('user').find(user);
    return usersCursor;
  }

  async updateAndReturnOne(args: {
    findUser: Partial<User>;
    updateUser: Partial<User>;
    method: '$set' | '$inc' | '$addToSet' | '$push';
  }): Promise<User> {
    const { findUser, updateUser, method } = args;
    const updateQuery = { [method]: updateUser };
    const userResponce = await this.getCollection.findOneAndUpdate(
      findUser,
      updateQuery,
      { returnDocument: 'after' },
    );
    return userResponce.value as User;
  }

  async updateOne(args: {
    findUser: Partial<User>;
    updateUser: Partial<Modify<User, { youInvited: ObjectId }>>;
    method: '$set' | '$inc' | '$addToSet' | '$push';
  }): Promise<void> {
    const { findUser, updateUser, method } = args;
    const updateQuery = { [method]: updateUser };
    const userResponce = await this.getCollection.updateOne(
      findUser,
      updateQuery,
    );
  }

  async insert(args: { user: User }) {
    const userResponce = await this.getCollection.insertOne(args.user);
    const user: User = {
      ...args.user,
      _id: userResponce.insertedId,
    } as User;
    return user;
  }

  async create(newUser: CreateUser): Promise<User | null> {
    const { avatar, dateOfBirth: _dateOfBirth, phoneNumber } = newUser;
    const filteredPhoneNumber = phoneNumber.replace(/\D/g, '');
    const photoURL =
      avatar &&
      (await this.photoUpload.uploadImageAWS(
        (await avatar).createReadStream(),
      ));
    const dateOfBirth = new Date(_dateOfBirth);
    const currentDate = new Date();

    const user: Partial<User> = {
      fullName: newUser.fullName,
      phoneNumber: filteredPhoneNumber,
      email: newUser.email,
      sex: newUser.sex,
      dateOfBirth,
      dateAdded: currentDate,
      photoURL,
    };

    const createUser = await this.updateAndReturnOne({
      findUser: { phoneNumber: user.phoneNumber },
      updateUser: user,
      method: '$set',
    });

    return createUser;
  }
}
