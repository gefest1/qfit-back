import { CACHE_MANAGER, Inject, UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ApolloError } from 'apollo-server-express';
import { Cache } from 'cache-manager';
import {
  CurrentUserGraph,
  PreAuthGuardGraph,
} from 'src/modules/admin/admin.guard';
import { TokenService } from 'src/modules/token/token.service';
import { filterObject } from 'src/utils/filterObject';
import { SMSservice } from 'src/utils/SMS/SMS.service';
import { CreditCardGraph } from '../models/creditCard.model';
import { CreateUser, User, UserGraph } from '../models/user.model';
import { UserService } from '../services/user.service';

@Resolver()
export class UserResolver {
  constructor(
    private userService: UserService,
    private smsService: SMSservice,
    private tokenService: TokenService,
    @Inject(CACHE_MANAGER) private cacheService: Cache,
  ) {}

  @Mutation(() => String)
  async sendVerificationSMS(
    @Args('phoneNumber', { type: () => String }) phoneNumber: string,
  ): Promise<string> {
    const code = '7777';
    const filteredPhoneNumber = phoneNumber.replace(/\D/g, '');
    this.smsService.sendVerificationSMS({
      code,
      phoneNumber: filteredPhoneNumber,
    });
    await this.cacheService.set(`${filteredPhoneNumber}`, `${code}`, {
      ttl: 300,
    });
    return 'success';
  }

  @Mutation(() => UserGraph)
  async checkSMSVerificationCode(
    @Args('phoneNumber', { type: () => String }) phoneNumber: string,
    @Args('code', { type: () => String }) code: string,
  ): Promise<UserGraph> {
    const filteredPhoneNumber = phoneNumber.replace(/\D/g, '');
    const originalCode = await this.cacheService.get(`${filteredPhoneNumber}`);
    if (code !== originalCode) throw new ApolloError('The code is wrong');

    const insertPhoneNumber = (
      await this.userService.getCollection.findOneAndUpdate(
        { phoneNumber: filteredPhoneNumber },
        {
          $set: {
            phoneNumber: filteredPhoneNumber,
          },
        },
        {
          upsert: true,
          returnDocument: 'after',
        },
      )
    ).value;
    const token = this.tokenService.create({
      user: {
        phoneNumber: insertPhoneNumber.phoneNumber,
        _id: insertPhoneNumber._id,
        dateAdded: new Date(),
      },
    });
    const userGraph: UserGraph = new UserGraph({
      ...insertPhoneNumber,
      creditCard: [],
      token,
    });
    return userGraph;
  }

  @Mutation(() => UserGraph)
  @UseGuards(PreAuthGuardGraph)
  async registerUser(@Args() args: CreateUser): Promise<UserGraph> {
    const user = await this.userService.create(args);
    const userResponce = new UserGraph({ ...user, creditCard: undefined });
    return userResponce;
  }

  @Mutation(() => UserGraph)
  @UseGuards(PreAuthGuardGraph)
  async editData(
    @Args('fullName', { nullable: true })
    fullName: string,
    @Args('email', { nullable: true })
    email: string,
    @CurrentUserGraph() user: User,
  ): Promise<UserGraph> {
    const _value = await this.userService.getCollection.findOneAndUpdate(
      {
        _id: user._id,
      },
      {
        $set: filterObject({ fullName, email }, ([_, value]) => {
          if (value === undefined) return false;
          return true;
        }),
      },
      {
        returnDocument: 'after',
      },
    );

    const userResponce = new UserGraph({
      ..._value.value,
      creditCard: undefined,
    });
    return userResponce;
  }

  @Query(() => UserGraph)
  @UseGuards(PreAuthGuardGraph)
  async getUser(@CurrentUserGraph() user: User): Promise<UserGraph> {
    return new UserGraph({
      ...user,
      creditCard: user?.creditCard?.map((e) => new CreditCardGraph(e)),
    });
  }
}
