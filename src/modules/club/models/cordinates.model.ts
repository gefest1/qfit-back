import { Field, InputType, ObjectType } from '@nestjs/graphql';

export interface Cordinates {
  lat: number;
  lng: number;
}

@ObjectType('Cordinates')
export class CordinatesGraph implements Cordinates {
  @Field()
  lat: number;

  @Field()
  lng: number;
}

@InputType()
export class CordinatesInp {
  @Field()
  lat: number;

  @Field()
  lng: number;
}
