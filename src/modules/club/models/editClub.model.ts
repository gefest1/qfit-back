import { ArgsType, Field, Float, GraphQLISODateTime } from '@nestjs/graphql';
import { FileUpload, GraphQLUpload } from 'graphql-upload';
import { Modify } from 'src/utils/modifyType';
import { Club } from './club.model';
import { ContactInput } from './contact.model';
import { CordinatesInp } from './cordinates.model';

@ArgsType()
export class EditClub
  implements
    Modify<
      Partial<Club>,
      {
        _id: string;
      }
    >
{
  @Field()
  _id: string;

  @Field({ nullable: true })
  name?: string;

  @Field({ nullable: true })
  address?: string;

  @Field({ nullable: true })
  city?: string;

  @Field({ nullable: true })
  description?: string;

  @Field(() => Float, { nullable: true })
  averagePrice?: number;

  @Field(() => GraphQLUpload, {
    name: 'photo',
    nullable: true,
  })
  photo?: Promise<FileUpload>;

  @Field(() => [GraphQLUpload], {
    name: 'gallery',
    nullable: true,
  })
  gallery?: Promise<FileUpload>[];

  @Field(() => CordinatesInp, { nullable: true })
  cordinates?: CordinatesInp;

  @Field(() => ContactInput, { nullable: true })
  contact?: ContactInput;
}
