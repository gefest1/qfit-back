import {
  ArgsType,
  Field,
  Float,
  GraphQLISODateTime,
  Int,
  ObjectType,
} from '@nestjs/graphql';
import { FileUpload, GraphQLUpload } from 'graphql-upload';
import { ObjectId } from 'mongodb';
import {
  PhotoUrl,
  PhotoUrlGraph,
} from 'src/modules/uploadPhoto/models/photo.model';
import { Modify } from 'src/utils/modifyType';
import { Contact, ContactGraph, ContactInput } from './contact.model';
import { Cordinates, CordinatesGraph, CordinatesInp } from './cordinates.model';

export interface Club {
  _id: ObjectId;
  name: string;
  dateAdded: Date;
  address: string;
  city: string;
  description: string;
  averagePrice: number;
  ratingSum?: number;
  ratingCount?: number;
  categories?: string[];
  photoURL: PhotoUrl;
  galleryURLs: PhotoUrl[];
  cordinates: Cordinates;
  contact: Contact;
}

@ObjectType('Club')
export class ClubGraph
  implements
    Modify<
      Club,
      {
        _id: string;
      }
    >
{
  @Field()
  _id: string;

  @Field()
  name: string;

  @Field(() => GraphQLISODateTime)
  dateAdded: Date;

  @Field()
  address: string;

  @Field({ nullable: true })
  city: string;

  @Field()
  description: string;

  @Field(() => Float)
  averagePrice: number;

  @Field(() => Float)
  ratingSum?: number;

  @Field(() => Float)
  ratingCount?: number;

  @Field(() => [String])
  categories?: string[];

  @Field(() => PhotoUrlGraph)
  photoURL: PhotoUrlGraph;

  @Field(() => [PhotoUrlGraph])
  galleryURLs: PhotoUrlGraph[];

  @Field(() => CordinatesGraph)
  cordinates: CordinatesGraph;

  @Field(() => ContactGraph)
  contact: ContactGraph;

  constructor(__: Partial<Club>) {
    if (__._id != null) this._id = __._id.toHexString();
    if (__.name != null) this.name = __.name;
    if (__.dateAdded != null) this.dateAdded = __.dateAdded;
    if (__.address != null) this.address = __.address;
    if (__.city != null) this.city = __.city;
    if (__.description != null) this.description = __.description;
    if (__.averagePrice != null) this.averagePrice = __.averagePrice;
    if (__.ratingSum != null) this.ratingSum = __.ratingSum;
    if (__.ratingCount != null) this.ratingCount = __.ratingCount;
    if (__.categories != null) this.categories = __.categories;
    if (__.cordinates != null) this.cordinates = __.cordinates;
    if (__.contact != null) this.contact = __.contact;
    if (__.photoURL != null) this.photoURL = __.photoURL;
    if (__.galleryURLs != null) this.galleryURLs = __.galleryURLs;
  }
}

@ArgsType()
export class CreateClub
  implements
    Modify<
      Partial<Club>,
      {
        _id?: string;
      }
    >
{
  @Field({ nullable: true })
  _id?: string;

  @Field()
  name: string;

  @Field()
  address: string;

  @Field()
  city: string;

  @Field()
  description: string;

  @Field(() => Float)
  averagePrice: number;

  @Field(() => GraphQLUpload, {
    name: 'photo',
  })
  photo: Promise<FileUpload>;

  @Field(() => [GraphQLUpload], {
    name: 'gallery',
  })
  gallery: Promise<FileUpload>[];

  @Field(() => CordinatesInp)
  cordinates: CordinatesInp;

  @Field(() => ContactInput)
  contact: ContactInput;
}
