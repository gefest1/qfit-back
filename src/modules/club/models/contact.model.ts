import { Field, InputType, ObjectType } from '@nestjs/graphql';

export interface Contact {
  email: string;
  phoneNumber: string;
}

@ObjectType('Contact')
export class ContactGraph implements Contact {
  @Field()
  email: string;
  @Field()
  phoneNumber: string;
}

@InputType()
export class ContactInput {
  @Field()
  email: string;
  @Field()
  phoneNumber: string;
}
