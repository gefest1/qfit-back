import { Args, Query, Resolver } from '@nestjs/graphql';
import { filterObject } from 'src/utils/filterObject';
import { ClubGraph } from '../models';
import { UserClubService } from '../services/user.service.club';

@Resolver()
export class UserClubResolver {
  constructor(public userClubService: UserClubService) {}

  @Query(() => [ClubGraph])
  async getClubs(
    @Args('city', { type: () => String, nullable: true }) city: string,
  ): Promise<ClubGraph[]> {
    const _searchPar = { city };

    const _result = filterObject(_searchPar, (arg) => {
      if (arg[1] === undefined) return false;
      return true;
    });

    const _clubs = await this.userClubService.getClubs(_result);

    return _clubs.map<ClubGraph>((e) => {
      return {
        ...e,
        _id: e._id.toHexString(),
      };
    });
  }
}
