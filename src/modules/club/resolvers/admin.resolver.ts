import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { ClubService } from '../services/service.club';
import { ClubGraph, CreateClub } from '../models';
import { EditClub } from '../models/editClub.model';
import { UseGuards } from '@nestjs/common';
import { PreAuthAdminGuardGraph } from 'src/modules/admin/admin.guard';

@Resolver()
@UseGuards(PreAuthAdminGuardGraph)
export class AdminClubResolver {
  constructor(public clubService: ClubService) {}

  @Mutation(() => ClubGraph)
  async createClubAdmin(@Args() args: CreateClub): Promise<ClubGraph> {
    const _club = await this.clubService.createClub(args);
    return new ClubGraph(_club);
  }

  @Mutation(() => ClubGraph)
  async editClubAdmin(@Args() args: EditClub): Promise<ClubGraph> {
    const _club = await this.clubService.editClub(args);
    return new ClubGraph(_club);
  }
}
