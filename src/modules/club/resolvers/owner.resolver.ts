import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import {
  CurrentUserGraph,
  PreAuthOwnerGuardGraph,
} from 'src/modules/admin/admin.guard';
import {
  PhotoUrl,
  PhotoUrlGraph,
  PhotoURLInput,
} from 'src/modules/uploadPhoto/models/photo.model';
import { Owner } from 'src/owner/models/owner.interface';
import { ClubGraph } from '../models';
import { EditClub } from '../models/editClub.model';
import { OwnerClubService } from '../services/owner.service.club';
import { ClubService } from '../services/service.club';

@Resolver()
@UseGuards(PreAuthOwnerGuardGraph)
export class OwnerClubResolver {
  constructor(
    private clubService: ClubService,
    private ownerClubService: OwnerClubService,
  ) {}

  @Mutation(() => ClubGraph)
  async editClubAdminOwner(@Args() args: EditClub): Promise<ClubGraph> {
    const _club = await this.clubService.editClub(args);
    console.log(_club.galleryURLs);
    return new ClubGraph(_club);
  }

  @Query(() => ClubGraph)
  async getClubAdminOwner(@CurrentUserGraph() user: Owner) {
    const club = await this.clubService.getClub({ _id: user.ownedClubID });
    const clubResponce = new ClubGraph({ ...club });
    return clubResponce;
  }

  @Mutation(() => ClubGraph)
  async deletePhotoGalleryClubOwner(
    @Args('galleryURLs', { type: () => [PhotoURLInput] })
    galleryURLs: PhotoURLInput[],
    @CurrentUserGraph() owner: Owner,
  ): Promise<ClubGraph> {
    const galleryURLsPhotos = galleryURLs.map((val) => {
      const url: PhotoUrl = {
        XL: val.XL,
        M: val.M,
        thumbnail: val.thumbnail,
      };
      return url;
    });
    const club = await this.ownerClubService.deletePhotoURLs({
      galleryURLs: galleryURLsPhotos,
      clubId: owner.ownedClubID,
    });
    const clubResponce = new ClubGraph({ ...club });
    return clubResponce;
  }
}
