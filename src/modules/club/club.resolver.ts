import { Resolver } from '@nestjs/graphql';
import { AWSPhotoUploadService } from '../uploadPhoto/awsSpace.service';

@Resolver()
export class ClubResolver {
  constructor(public photoUpload: AWSPhotoUploadService) {}
}
