import { Module } from '@nestjs/common';
import { OwnerModule } from 'src/owner/owner.module';
import { PreAuthModule } from '../admin/admin.module';

import { AWSPhotoUploadModule } from '../uploadPhoto/awsSpace.module';
import { UserModule } from '../user/user.module';
import { ClubResolver } from './club.resolver';
import { AdminClubResolver } from './resolvers/admin.resolver';
import { OwnerClubResolver } from './resolvers/owner.resolver';
import { UserClubResolver } from './resolvers/user.resolver';
import { OwnerClubService } from './services/owner.service.club';
import { ClubService } from './services/service.club';
import { UserClubService } from './services/user.service.club';

@Module({
  imports: [AWSPhotoUploadModule, PreAuthModule],
  providers: [
    ClubService,
    UserClubService,
    AdminClubResolver,
    OwnerClubResolver,
    UserClubResolver,
    ClubResolver,
    OwnerClubService,
  ],
  exports: [AdminClubResolver, OwnerClubResolver, UserClubResolver],
})
export class ClubModule {}
