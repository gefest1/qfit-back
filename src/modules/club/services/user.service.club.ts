import { Inject, Injectable } from '@nestjs/common';
import { Db, Collection } from 'mongodb';
import { AWSPhotoUploadService } from 'src/modules/uploadPhoto/awsSpace.service';
import { Club } from '../models';

@Injectable()
export class UserClubService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
    public photoUpload: AWSPhotoUploadService,
  ) {}

  get clubCollection(): Collection<Club> {
    return this._mongodDb.collection('club');
  }

  async getClubs(args: Partial<Club>): Promise<Club[]> {
    return !Object.entries(args).length
      ? await this.clubCollection.find().toArray()
      : await this.clubCollection.find(args).toArray();
  }
}
