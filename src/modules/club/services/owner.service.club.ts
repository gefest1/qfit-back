import { Inject, Injectable } from '@nestjs/common';
import { Collection, Db, ObjectId } from 'mongodb';
import {
  PhotoUrl,
  PhotoUrlGraph,
} from 'src/modules/uploadPhoto/models/photo.model';
import { Club } from '../models/club.model';

@Injectable()
export class OwnerClubService {
  constructor(@Inject('DATABASE_CONNECTION') private _mongoDb: Db) {}

  get clubCollection(): Collection<Club> {
    return this._mongoDb.collection('club');
  }

  async deletePhotoURLs(args: {
    galleryURLs: PhotoUrl[];
    clubId: ObjectId;
  }): Promise<Club> {
    const { galleryURLs, clubId } = args;
    const clubUpdate = await this.clubCollection.findOneAndUpdate(
      { _id: clubId },
      {
        $pullAll: { galleryURLs: galleryURLs },
      },
      { returnDocument: 'after' },
    );
    const { value, ok } = clubUpdate;
    console.log(ok);
    return value;
  }
}
