import { Inject, Injectable } from '@nestjs/common';
import { Db, ObjectId, Collection } from 'mongodb';
import { AWSPhotoUploadService } from 'src/modules/uploadPhoto/awsSpace.service';
import { Club, CreateClub } from '../models';
import { EditClub } from '../models/editClub.model';

@Injectable()
export class ClubService {
  constructor(
    @Inject('DATABASE_CONNECTION')
    public _mongodDb: Db,
    public photoUpload: AWSPhotoUploadService,
  ) {}

  get clubCollection(): Collection<Club> {
    return this._mongodDb.collection('club');
  }

  async getClub(args: Partial<Club>) {
    const objectLength = Object.entries(args).length;
    const club = !objectLength
      ? await this.clubCollection.findOne()
      : await this.clubCollection.findOne(args);
    return club;
  }

  async createClub(newClub: CreateClub): Promise<Club> {
    const [photoURL, galleryURLs] = await Promise.all([
      this.photoUpload.uploadImageAWS((await newClub.photo).createReadStream()),
      Promise.all(
        newClub.gallery.map(async (_val) =>
          this.photoUpload.uploadImageAWS((await _val).createReadStream()),
        ),
      ),
    ]);
    const club: Omit<Club, '_id'> & {
      _id?: ObjectId;
    } = {
      address: newClub.address,
      averagePrice: newClub.averagePrice,
      city: newClub.city,
      contact: newClub.contact,
      cordinates: newClub.cordinates,
      dateAdded: new Date(),
      description: newClub.description,
      galleryURLs,
      photoURL,
      name: newClub.name,
      categories: [],
      ratingCount: 0,
      ratingSum: 0,
      _id: newClub._id && new ObjectId(newClub._id),
    };

    const result = await this.clubCollection.insertOne(club);

    return {
      ...club,
      _id: new ObjectId(result.insertedId.toHexString()),
    };
  }

  async editClub(editClub: EditClub): Promise<Club> {
    const { _id, gallery, photo, ..._editClubNote } = editClub;
    const _editClub: Partial<Club> = _editClubNote;
    const [photoURL, galleryURLs] = await Promise.all([
      photo &&
        this.photoUpload.uploadImageAWS((await photo).createReadStream()),
      gallery &&
        gallery.length &&
        Promise.all(
          gallery.map(async (_val) =>
            this.photoUpload.uploadImageAWS((await _val).createReadStream()),
          ),
        ),
    ]);
    const updateQuery = {
      $set: _editClub,
    };
    if (photoURL) {
      _editClub.photoURL = photoURL;
    }
    if (galleryURLs) {
      updateQuery['$push'] = { galleryURLs: { $each: galleryURLs } };
      console.log(updateQuery);
    }
    const editedClub = await this.clubCollection.findOneAndUpdate(
      {
        _id: new ObjectId(_id),
      },
      updateQuery,
      {
        returnDocument: 'after',
      },
    );
    return editedClub.value;
  }
}
