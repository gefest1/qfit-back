import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Resolver } from '@nestjs/graphql';
import {
  CurrentUserGraph,
  PreAuthGuardGraph,
} from 'src/modules/admin/admin.guard';
import {
  CreditCardGraph,
  CreditCardInput,
} from 'src/modules/user/models/creditCard.model';
import { User } from 'src/modules/user/models/user.model';
import { SavePaymentService } from 'src/modules/user/services/savePayment.service';

@Resolver()
@UseGuards(PreAuthGuardGraph)
export class PaymentResolver {
  constructor(public savePaymentService: SavePaymentService) {}

  @Mutation(() => CreditCardGraph)
  async saveCard(
    @Args()
    creditCardInput: CreditCardInput,
    @CurrentUserGraph() user: User,
  ): Promise<CreditCardGraph> {
    const { cardHolderName, cardToken, cardType, last4Digits } =
      creditCardInput;
    const creditCard = await this.savePaymentService.saveCard({
      cardHolderName,
      cardToken,
      cardType,
      last4Digits,
      userId: user._id,
    });

    return new CreditCardGraph(creditCard);
  }
}
