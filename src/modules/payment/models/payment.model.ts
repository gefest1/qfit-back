export interface PaymentModel {
  Model?: {
    TransactionId?: number;
    Amount?: number;
    Currency?: string;
    CurrencyCode?: number;
    InvoiceId?: string;
    AccountId?: string;
    Email?: string;
    Description?: string;
    JsonData?: any;
    CreatedDate?: Date;
    CreatedDateIso?: string;
    AuthDate?: Date;
    AuthDateIso?: string;
    ConfirmDate?: Date;
    ConfirmDateIso?: string;
    AuthCode?: string;
    TestMode?: boolean;
    IpAddress?: string;
    IpCountry?: string;
    IpCity?: string;
    IpRegion?: string;
    IpDistrict?: string;
    IpLatitude?: number;
    IpLongitude?: number;
    CardFirstSix?: string;
    CardLastFour?: string;
    CardType?: string;
    CardTypeCode?: number;
    Issuer?: string;
    IssuerBankCountry?: string;
    Status?: string;
    StatusCode?: number;
    Reason?: string;
    ReasonCode?: string;
    CardHolderMessage?: string; //сообщение для покупателя
    Name?: string;
    Token?: string;
  };
  Success: boolean;
  Message: string;
}
