import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { SavePaymentService } from '../user/services/savePayment.service';
import { UserModule } from '../user/user.module';
import { PaymentResolver } from './resolvers/payment.resolver';
import { PaymentService } from './services/payment.service';

@Module({
  imports: [HttpModule, UserModule],
  providers: [PaymentService, PaymentResolver, SavePaymentService],
  exports: [PaymentService],
})
export class PaymentModule {}
