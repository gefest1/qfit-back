import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ObjectId } from 'mongodb';
import { lastValueFrom } from 'rxjs';
import { PaymentModel } from '../models/payment.model';

const Authorization =
  'Basic cGtfYjJiZmQ3MGE1MDcyNzJlZjNhZTRmMGE5N2ZkMTQ6N2M2NDQwYzg5MDE0MDg1YWVjNWEwMjcxMGQ0MDlmMDI=';

@Injectable()
export class PaymentService {
  // constructor() // public httpService: HttpService
  // {}

  async authPayment(__: {
    userId: ObjectId;
    token: string;
    amount: number;
  }): Promise<PaymentModel> {
    const { userId: _userId, token, amount } = __;
    const userId = _userId.toHexString();
    const _result: PaymentModel = {
      Model: {
        TransactionId: 0,
        AccountId: userId,
        Amount: amount,
        Currency: 'KZT',
        Token: token,
      },
      Success: true,
      Message: 'GOOD',
    };
    // await lastValueFrom(
    //   this.httpService.post<PaymentModel>(
    //     'https://api.cloudpayments.ru/payments/tokens/auth',
    //     {
    //       AccountId: userId,
    //       Amount: amount,
    //       Currency: 'KZT',
    //       Token: token,
    //     },
    //     {
    //       headers: {
    //         Authorization,
    //       },
    //     },
    //   ),
    // );
    if (!_result.Success) throw _result.Message || 'SOMETHING WENT WRONG';
    return _result;
  }

  async confirmPayment(__: {
    TransactionId: number;
    Amount: number;
  }): Promise<PaymentModel> {
    const { TransactionId, Amount } = __;
    const _result: PaymentModel = {
      Model: {
        TransactionId: TransactionId,
        Amount: Amount,
        // Amount: amount,
        // Currency: 'KZT',
        // Token: token,
      },
      Success: true,
      Message: 'GOOD',
    };
    // await lastValueFrom(
    //   this.httpService.post<PaymentModel>(
    //     'https://api.cloudpayments.ru/payments/confirm',
    //     {
    //       TransactionId: TransactionId,
    //       Amount: Amount,
    //     },
    //     {
    //       headers: {
    //         Authorization,
    //       },
    //     },
    //   ),
    // );
    if (!_result.Success) throw _result.Message;
    return _result;
  }

  async chargePayment(__: { userId: string; Token: string; Amount: number }) {
    const { userId, Token, Amount } = __;

    const _result: PaymentModel = {
      Model: {
        AccountId: userId,
        Amount,
        Currency: 'KZT',
        Token,
      },
      Success: true,
      Message: 'TRUE',
    };

    if (!_result.Success) throw _result.Message;
    return _result;
  }

  async cancelPayment(__: { TransactionId: number }) {
    const { TransactionId } = __;
    return true;
    // const _result: PaymentModel = {
    //   Success: true
    // };
    // await lastValueFrom(
    //   this.httpService.post<PaymentModel>(
    //     'https://api.cloudpayments.ru/payments/void',
    //     {
    //       TransactionId: TransactionId,
    //     },
    //     {
    //       headers: {
    //         Authorization,
    //       },
    //     },
    //   ),
    // );
    // if (!_result.data.Success) throw _result.data.Message;
    // return _result.data;
  }
}
