import { CacheModule, Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { AppController } from './app.controller';

import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import { MongoDBModule } from './utils/mongo/mongo.module';
import { ClubModule } from './modules/club/club.module';
import { AWSPhotoUploadModule } from './modules/uploadPhoto/awsSpace.module';
import { UserModule } from './modules/user/user.module';
import { ServiceModule } from './modules/service/service.module';
import { OwnerModule } from './owner/owner.module';

import { BookingModule } from './modules/booking/booking.module';
import { SessionModule } from './modules/Session/session.module';
import { PaymentModule } from './modules/payment/payment.module';
import { PromocodeModule } from './modules/promocode/promocode.module';
import { StatistcModule } from './modules/statistcs/statistc.module';
import { EmailModule } from './modules/email/email.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    HttpModule,
    CacheModule.register(),
    GraphQLModule.forRoot({
      debug: true,
      playground: true,
      include: [],
      autoSchemaFile: true,
      sortSchema: true,
      uploads: false,
      resolvers: {},
      context: ({ req, res }) => ({ req, res }),
    }),
    AWSPhotoUploadModule,
    UserModule,
    ClubModule,
    MongoDBModule,
    ServiceModule,
    OwnerModule,
    BookingModule,
    SessionModule,
    PaymentModule,
    PromocodeModule,
    StatistcModule,
    EmailModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
